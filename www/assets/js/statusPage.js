/*global document, $, console, moment */
var tokenPromise = $.Deferred(),
    token = 'HOLDER',
    latestApp = '1.9.6';


$.get('https://tracker.kort.fo/api/token', function (data) {
    "use strict";
    token = encodeURIComponent(data.token);
    tokenPromise.resolve();
});

moment.lang('fo');

function DeviceStatusApp(obj) {
    "use strict";

    this.versionCode = obj.versionCode;
    this.versionName = obj.versionName;

    this.isLatest = function () {
        return this.versionName === latestApp;
    };
}

function DeviceStatus(deviceStatusObj) {
    "use strict";

    this.socketStatus = deviceStatusObj.socketStatus;
    this.app = new DeviceStatusApp(deviceStatusObj.app);
    this.GPS = deviceStatusObj.GPS;
    this.GSM = deviceStatusObj.GSM;
    this.time = new Date(deviceStatusObj.time);
    this.device = deviceStatusObj.device;
    this.pauseInfo = deviceStatusObj.pauseInfo;


    this.getBatteryCell = function () {
        var $cell = $('<td />');
        if (this.device) {
            if (this.device.batteryLevel < 35 && this.device.batteryLevel > 15) {
                $cell.addClass('warning');
            }
            if (this.device.batteryLevel < 15) {
                $cell.addClass('error');
            }
            $cell.append(this.device.batteryLevel.toFixed(0) + "%");
        }
        return $cell;
    };
}

/**
 * Device.ready
 */
function Device(device) {
    "use strict";
    // Privates
    var self = this;

    // Public

    this.id = device.id;
    this.name = device.name;
    this.ready = new $.Deferred();


    this.isLommy = undefined;
    this.lastActivity = {
        statusReport: false,
        point: false,
        getLastStatusReportCell: function () {
            var $cell = $('<td/>');
            if (this.statusReport) {
                if (moment().diff(this.statusReport) > 1000 * 60 * 60) {
                    $cell.addClass('warning');
                }

                $cell.append(moment(this.statusReport).fromNow());
            }
            return $cell;
        },
        getLastPointCell: function () {
            var $cell = $('<td />');
            if (this.point) {
                if (moment().diff(this.point) > 1000 * 60 * 60 && !this.pause) {
                    $cell.addClass('warning');
                }


                $cell.append(moment(this.point).fromNow());
            }
            return $cell;
        }
    };
    this.getBatteryCell = function () {
        if (this.deviceStatus) {
            return this.deviceStatus.getBatteryCell();
        }
        return $('<td/>');
    };

    this.getSlagCell = function () {
        var $cell = $('<td />'),
            slag = "";

        if (this.isLommy) {
            slag = "Lommy";
        } else if (this.statusMsg && this.statusMsg.deviceStatus.app) {
            slag = "Android V. " + this.statusMsg.deviceStatus.app.versionName;
        } else {
            slag = "N/A";
        }

        $cell.append(slag);

        if (this.statusMsg.deviceStatus && !this.statusMsg.deviceStatus.app.isLatest()) {
            //  $cell.addClass('warning');
            $cell.append($('<i />').addClass('icon-warning-sign'));
        }

        return $cell;

    };

    this.hasLatestApp = function () {
        return this.statusMsg.deviceStatus.app.versionName === latestApp;
    };

    this.getStatusRow = function (callback) {
        var $row = $('<tr />');

        $row.append($('<td>' + this.name + '</td>'));
        $row.append(this.getSlagCell());
        $row.append($('<td>' + this.id + '</td>'));
        $row.append(this.lastActivity.getLastStatusReportCell());
        $row.append(this.lastActivity.getLastPointCell());
        $row.append($('<td>' + (this.lastActivity.pause ? "Ja" : "Nei") + '</td>'));
        $row.append(this.getBatteryCell());



        return $row;
    };

    /**
     * Initialization code for device
     */
    (function () {
        var isLommyReq = $.get('//tracker.kort.fo/api/device/isLommy/' + self.id),
            statusMsgReq = $.get('//tracker.kort.fo/api/device/status/' + self.id + '.json'),
            pointReq = $.get('//tracker.kort.fo/api/line/' + self.id + '/2');


        $.when(statusMsgReq, isLommyReq, pointReq).then(function (statusMsg, lommyReq, pointReq) {
            // Success

            if (statusMsg[0].deviceStatus) {
                statusMsg[0].deviceStatus = new DeviceStatus(statusMsg[0].deviceStatus);
            }

            self.isLommy = lommyReq[0];
            self.statusMsg = statusMsg[0];
            self.deviceStatus = statusMsg[0].deviceStatus;
            self.lastActivity.statusReport = statusMsg[0].deviceStatus.time;
            if (pointReq[0].paths[0][0]) {
                self.lastActivity.point = new Date(pointReq[0].paths[0][0][2]);
                self.lastActivity.pause = (pointReq[0].paths[0][0][6] === "stop");
            }
            self.ready.resolve();
            
        }, function () {
            // Failure
        });
    }());

}



$(document).ready(function () {
    "use strict";

    tokenPromise.done(function () {
        $.get(
            '//tracker.kort.fo/api/devices.json?token=' + token + '&fields=locations',
            function (data) {
                var $tbody = $('.unitList').find('tbody');
                $tbody.parent().tablesorter();

                data.devices
                    .map(function (device) {
                        return new Device(device);
                    })
                    .forEach(function (device) {
                        device.ready.then(function () {
                            $tbody.append(device.getStatusRow());
                            $tbody.parent()
                                .trigger('update')
                                .trigger("sorton", [ [[0, 0], [0, 0]] ]);

                        });
                    });
            }
        );
    });
});
