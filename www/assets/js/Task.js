/*global console, require, define, $, document, mtData, dojo, tasks, map */
function TaskStatus(state) {
    "use strict";
    this.system_status = state.system_status;
    this.description = state.description;
    this.datetime = new Date(state.datetime);
    this.getDescription = function () {
        return (this.datetime.toDDMM() + ', ' + this.datetime.toHHMM() + ' ' + this.description);
    };
}

function Task(task) {
    "use strict";

    /**
     * Private variables
     */
    var self = this,
        graphicsLayer;

    this.task = task.task;
    this.created = new Date(task.task.created);

    /**
     * Task methods
     */

    // ARRAY
    this.getStatuses = function () {
        if (this.task.statuses) {
            return this.task.statuses.map(function (state) {
                return new TaskStatus(state);
            });
        }
        return [];
    };

    // BOOL
    this.isEnded = function () {
        return this.getStatuses().filter(function (taskStatus) {
            return (taskStatus.system_status === "END");
        }).length > 0;
    };

    // BOOL
    this.isStarted = function () {
        return this.getStatuses().filter(function (taskStatus) {
            return (taskStatus.system_status === "START");
        }).length > 0;
    };

    // BOOL
    this.equals = function (task) {
        /**
         * comparision by taskID would be better.
         */
        if (JSON.stringify(this.task) === JSON.stringify(task.task)) {
            return true;
        }
        return false;
    };

    // BOOL
    this.equalsById = function (task) {
        return (this.task.id === task.id);
    };

    // VOID
    this.merge = function (updated_task, cb) {
        console.log("Updating task", this.task, "with data from ", updated_task);
        this.task.statuses = updated_task.statuses;
        cb(this);
    };

    // MapPoint
    this.getCenteroid = function (callback) {
        console.log("in getcenteroid, before require ");
        require(['esri/geometry/Point',
            'esri/geometry/Multipoint'],
            function (Point, MultiPoint) {
                console.log("in getcenteroid, before callback");
                var pt = new MultiPoint(map.spatialReference);
                self.task.locations.forEach(function (loc) {
                    pt.addPoint(new Point(loc));
                });

                callback(pt.getExtent().getCenter());
            });
    };

    /**
     * Public elements
     */

    // I suggest to make this private
    this.htmlElement = null;
    this.updateHtmlElement = function () {
        // Handlebars will be used here
        var self = this;
        if (this.htmlElement !== null) {
            $('ul li', this.htmlElement).remove();

            $(this.task.statuses).each(function (i, state) {
                $('ul', this.htmlElement).append('<li>' +
                    new Date(state.datetime).toDDMM() +
                    ', ' + new Date(state.datetime).toHHMM() +
                    ' ' + state.description + "</li>");
            });
            $('.device', this.htmlElement).empty();
            $(this.task.devices).each(function (i, device) {
                $('.device', self.htmlElement).append(self.getDeviceDesc(device));
            });
        }
    };

    this.getDomNode = function () {
        this.show();
        return this.htmlElement;
    };

    this.isVerified = function (deviceId) {
        var verified = false;
        $(this.task.devices).each(function (i, device) {
            if (device.hasOwnProperty('accepted') && device.id === deviceId && device.accepted) {
                verified = device.accepted;
            }
        });
        return verified;
    };

    this.showLocationsOnMap = function (map) {
        // Wondering if it is possible to use .call/.apply instead of a self variable
        var self = this;
        require([
            'esri/layers/GraphicsLayer',
            'esri/graphic',
            'esri/geometry/Point',
            'esri/symbols/SimpleMarkerSymbol',
            'esri/symbols/SimpleLineSymbol',
            'dojo/_base/Color'],
            function (GraphicsLayer, Graphic, Point, SimpleMarkerSymbol, SimpleLineSymbol, Color) {
                graphicsLayer = new GraphicsLayer();
                console.log("Task.showLocationsOnMap: task", self);
                self.task.locations.forEach(function (loc) {
                    graphicsLayer.add(new Graphic(new Point(loc.x, loc.y), new SimpleMarkerSymbol(
                        SimpleMarkerSymbol.STYLE_CIRCLE,
                        15,
                        new SimpleLineSymbol(),
                        new Color([255, 0, 0])
                    )));
                });

                map.addLayer(graphicsLayer);
            });
    };

    this.hideLocationsOnMap = function (map) {
        if (graphicsLayer && map) {
            map.removeLayer(graphicsLayer);
            graphicsLayer = false;
        }
    };

    this.getDeviceDesc = function (device) {
        var symbol = document.createElement('div'),
            deviceElem = document.createElement('div'),
            deviceName = document.createElement('div');

        $(symbol).addClass('symbol');

        // At skriva um till promise/deferred: 
        if (mtData.getDevice(device.id)) {
            symbol.style.background = mtData.getDevice(device.id).color.toCss(true);
        }


/**

        if (mtData !== undefined && mtData.devices[device.id] !== undefined &&
                mtData.devices[device.id].hasOwnProperty('color')) {
            symbol.style.background = mtData.devices[device.id].color.toCss(true);
            console.log("Setting bg color of symbol to ", mtData.devices);
        } else {
            $(document).one('colors_ready', function () {
                if (mtData.devices[device.id] !== undefined &&
                        mtData.devices[device.id].hasOwnProperty('color')) {
                    symbol.style.background = mtData.devices[device.id].color.toCss(true);
                }
            });
        }
        */

        $(deviceElem).addClass('device').append(symbol);
        if (tasks.devices.find(device.id) !== undefined) {
            $(deviceName).append(tasks.devices.find(device.id).name).addClass('deviceName');
        }
        if (this.isVerified(device.id)) {
            $(deviceElem).append("<i class='icon-white icon-ok-sign' style='margin-left: 5px; margin-top: 1px;' />");
        }
        $(deviceElem).append(deviceName);
        return deviceElem;
    };
    this.show = function () {
        var taskElem = dojo.doc.createElement('div'),
            description = dojo.doc.createElement('div'),
            statuses = dojo.doc.createElement('ul'),
            datetime = $('<div />').addClass('datetime'),
            deleteButton = $('<i class="deleteTask icon-white icon-remove"></i>');

        this.htmlElement = taskElem; // Beware of memory leaks! binding directly to DOM

        $(description)
            .append(this.task.description)
            .addClass('description');

        $(taskElem)
            .addClass('task')
            .append(description)
            .append(deleteButton);

        $(this.task.devices).each(function (i, device) {
            $(taskElem).append(self.getDeviceDesc(device));
        });
        this.getStatuses().forEach(function (state) {
            var $stateElem = $('<li/>');
            $stateElem.append(state.getDescription());
            $(statuses).append($stateElem);
        });
        $(datetime).append('Dagfesting: ', this.created.toDDMM() + " " + this.created.toHHMM());

        $(datetime).hide();
        $(statuses).hide();
        $(taskElem).append(datetime, statuses);
        $(deleteButton).hide();

        /**
         * This handler gets detached for all tasks 
         * when status is updated by a device
         *
         * Also when a task is deleted.
         */
        $(taskElem).click(function () {
            console.log("Show or hide task info");
            $(datetime).toggle();
            $(statuses).toggle();
            $(deleteButton).toggle();
            $(taskElem).toggleClass('expanded');
            self.getCenteroid(function (center) {
                console.log("Center at ", center);
                if (center) {
                    map.centerAt(center);
                }
            });
        });

        $(deleteButton).click(function () {
            $(taskElem).hide();
            tasks.deleteTask(self, function (a, b, c) {
                self.hideLocationsOnMap(map);
                $('.tasks').trigger('tasksupdated');
            });
        });
    };
}

define(['/assets/js/Task.js'], function () {
    "use strict";
    return Task;
});

