/*global console, $, Tasks */
Tasks.prototype.create = {
    initModal: false,
    save: function (task, success, failure) {
        "use strict";
        $.ajax({
            type: 'POST',
            url: '/api/tasks',
            data: task,
            success: success,
            failure: failure
        });
    },
    showModal: function (element) {
        "use strict";
        if (!this.initModal) {
            this.initModal = true;
            var overlay = $('#overlay').overlay({
                mask: {
                    color: '#000',
                    loadSpeed: 200,
                    opacity: 0.5
                },
                onBeforeLoad: function () {
                    this.getOverlay().find(".contentWrap").load('createTask.html');
                },
                onLoad: function () {
                    $('.createTask form').submit(function (event) {

                        console.log($(this).serialize());
                        $.ajax({
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            type: 'POST',
                            success: function (result) {
                                if (result.success) {
                                    $(document).trigger('taskOverlayClose');
                                    $(document).trigger('dataChanged');
                                }
                            }
                        });
                        console.log(event);
                        return event.preventDefault();
                    });
                },
                load: true
            });
        } else {
            $('#overlay').overlay().load();
        }
    }
};
