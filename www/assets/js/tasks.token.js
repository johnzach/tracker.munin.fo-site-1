/*global Tasks, $, document, console*/
Tasks.prototype.token = {
    get: function (callback) {
        "use strict";
        $.ajax({
            url: '/api/token',
            success: function (result) {
                callback(result);
            },
            error: function (error) {
                console.error(error);
            }
        });
    },
    init: function () {
        "use strict";
        var self = this;
        this.get(function (token) {
            self.token = token.token;
            $(document).trigger('task_token');
        });
    },
    token: null
};
