// From: http://stackoverflow.com/questions/1960473/unique-values-in-an-array
Array.prototype.getUnique = function () {
    "use strict";
    var i, l, u = {}, a = [];
    for (i = 0, l = this.length; i < l; ++i) {
        if (u.hasOwnProperty(this[i])) {
            continue;
        }
        a.push(this[i]);
        u[this[i]] = 1;
    }
    return a;
}

String.prototype.intersection = function (anotherString) {
    "use strict";
    function createGrid(rows, columns) {
        var grid = [rows], i, j;
        for (i = 0; i < rows; i += 1) {
            grid[i] = [columns];
            for (j = 0; j < columns; j += 1) {
                grid[i][j] = 0;
            }
        }
        return grid;
    }
    var grid = createGrid(this.length, anotherString.length), longestSoFar = 0, matches = [], i, j, match;

    for (i = 0; i < this.length; i += 1) {
        for (j = 0; j < anotherString.length; j += 1) {
            if (this.charAt(i).toLowerCase() === anotherString.charAt(j).toLowerCase()) {
                if (i === 0 || j === 0) {
                    grid[i][j] = 1;
                } else {
                    grid[i][j] = grid[i - 1][j - 1] + 1;
                }
                if (grid[i][j] > longestSoFar) {
                    longestSoFar = grid[i][j];
                    matches = [];
                }
                if (grid[i][j] === longestSoFar) {
                    match = this.substring(i - longestSoFar + 1, i);
                    matches.push(match);
                }
            }
        }
    }
    return matches.toString();
};
Date.prototype.toHHMM = function () {
    "use strict";
    return (
        (this.getHours() < 10 ? '0' + this.getHours() : this.getHours()) +  ':' +
        (this.getMinutes() < 10 ? '0' + this.getMinutes() : this.getMinutes())
    );
};
Date.prototype.toDDMM = function () {
    "use strict";
    return (
        (this.getDate() < 10 ? '0' + this.getDate() : this.getDate()) +  '/' +
        (this.getMonth() + 1 < 10 ? '0' + (this.getMonth() + 1) : this.getMonth() + 1)
    );
};

// http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
String.prototype.hashCode = function(){
    var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

