<?php
	require_once('../admin/.db_connect.php');

	$name = pg_escape_string($_POST['name']);
	$user_name = pg_escape_string($_POST['user_name']);
	$email = pg_escape_string($_POST['email']);
	$password = pg_escape_string($_POST['password']);

	// Here should I check if I have permissions to create a user

	switch($_POST['type']) {
		case 'login':
			$user = login($email, $password);
			break;
		case 'register':
			if ($_POST['password'] != $_POST['password_verification']) {
				die('Passwords do not match');
			}
			$user = create_user($user_name, $email, $password);
			break;
		default:
			die('<h1>No access</h1>');

	}
	$symb = generate_symbology();
	add_device($user, $symb, $name);

	function login($email, $password) {
		$sql = 'SELECT "ID"
			FROM "User"
			WHERE lower("Email") = lower(\''.$email.'\')
			AND "Password" = \''.sha1($password).'\'';
		$query = pg_query($sql) or die('Query failed');
		if (pg_num_rows($query) == 0) {
			die('Wrong email or password...');
		}
		return(current(pg_fetch_row($query)));
	}

	function create_user($name,$email,$password) {
		$sql = 'INSERT INTO "User" ("Name", "Email", "Password")
			VALUES (\''.$name.'\', lower(\''.$email.'\'), \''.sha1($password).'\')
			RETURNING "ID"';

		$query = pg_query($sql) or die("Query error " . $name . $email . pg_last_error());

		return(current(pg_fetch_row($query)));
	}
	
	function generate_symbology() {
		$rgb = "{102,102,255}";

		$result = pg_query('INSERT INTO "Symbology" ("RGB") 
				    VALUES (\''.$rgb.'\')
				    RETURNING "ID"');
		if (!$result)
			die("symb - Database query error ".pg_last_error());

		return(current(pg_fetch_row($result)));
	}
	function add_device($userID, $SymbologyID,$name) {
		$result = pg_query('INSERT INTO "Device" (
				"UserID", 
				"Name", 
				"IMEI", 
				"Tlf",
				"SymbologyID"
			)
			VALUES(
				'.intval($userID).', 
				\''.$name.'\',
				\''.pg_escape_string($_POST['deviceid']).'\', 
				'.intval($_POST['tlfnr']).',
				'.intval($SymbologyID).'
			);');

		if (!$result)
			die("add - Database query error". pg_last_error());
	
	}
?>
<h1>Brúkari skrásettur.</h1><?php /* DO NOT CHANGE - ANDROID APP DEPENDS ON THIS.. */ ?>
Steðga munintracker og royn aftur.
