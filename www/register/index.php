<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <title>Skráset eind</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" href="Toast/lib/toast.css">
  <link rel="stylesheet" href="Toast/style.css">
</head>

<body>
    <header role="banner">
        <div class="wrap">
            <a id="logo" href="#">Skráset eind</a>
  	</div>
    </header>

    <div role="content">
        <div class="wrap">
  		<div class="grids">
			<!--<?php if (isset($_GET['id'])) echo "<div class='ooc-message error'>Tað sær út til at eindin hjá tær ikki er skrásett. Her er ein formularur at skráseta hana við</div>"; ?>-->
			<!--<p> Ert tú skásettur? <a class="butt yellow" href="">INNRITA</a></p> -->
			<form action="newuser.php" method="post" id="signup_form">
			    <label for="name">Navn <em>*</em></label>
			    <input name="name" id="name" class="required" />

			    <label for="tlfnr">Telefonnummar <em>*</em></label>
			    <input name="tlfnr" class="required" />

			    <label for="email">Teldupostur <em>*</em></label>
			    <input name="email" id="email" type="email" class="required" />

			    <label for="password">Loyniorð <em>*</em></label>
			    <input name="password" id="password" type="password" class="required" />

                            <span id="registercontrols" style="display: none;">
				<label for="password">Loyniorð umaftur <em>*</em></label>
				<input name="password_verification" type="password" />

				<label for="name">Kontunavn <em>*</em></label>
				<input name="user_name" id="name" />
                            </span>

			    <input type="hidden" name="deviceid" value="<?=$_GET['id']?>" />
			    <input type="hidden" name="type" value="login" id="type" />
			    <input type="submit" name"submit" value="Innrita" class="butt blue" id="submitbutton" />
			</form>
  		</div>
  	</div>
  </div>

  <footer role="footer">
  	<div class="wrap">
  		
  	</div>
  </footer>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
  <script>
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if( !emailReg.test( $email ) ) {
            return false;
        } else {
            return true;
        }
    }
    $('#signup_form').submit(function(e) {
        var error;
        $('.required').each(function(index,elem) {
            if (elem.value.length < 2) {
                $(elem).addClass('error');
                error = true;
            }
        });
        if (typeof error !== "undefined" && error === true) {
            e.preventDefault();
        }
    });
    $('#email').keyup(function(e) {
        delay(function() {
            if (validateEmail(e.srcElement.value) && e.srcElement.value.indexOf('@') !== -1) {
		console.log(e.srcElement.value);
		console.log(validateEmail(e.srcElement.value));
                $.get('kontafinnst.php', {email: e.srcElement.value}, function(data) {
                    console.log(data);
                    if (data == 'true') {
                        $('#registercontrols').css('display', 'none');
                        $('#submitbutton').val('Innrita');
                        $('#type').val('login');
                    } else {
                        $('#registercontrols').css('display', '');
                        $('#submitbutton').val('Skráset');
                        $('#type').val('register');
                    }
                });
            }
        },300);
    });


  </script>
  <!--[if lt IE 7 ]>
    <script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
</body>
</html>
