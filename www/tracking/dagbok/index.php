<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="http://serverapi.arcgisonline.com/jsapi/arcgis/2.6/js/dojo/dijit/themes/claro/claro.css" />
    <link rel="stylesheet" href="/tracking/style.css" media="all" />
    <script>
        dojoConfig = { parseOnLoad: true};
    </script>
    <script src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=2.6"></script>
    <script>
        dojo.require("dijit.dijit");
        dojo.require("dijit.Calendar");

        var device = {
            id: 26
        }
var a,b;
        dojo.ready(function() {
            var xhrArgs = {
                url: "date.php",
                handleAs: "json",
                content: {
                    device: device.id,
                },
                load: function(data){
a=data;
                    console.log(data);
                    new dijit.Calendar({
                        value: new Date(),
                        isDisabledDate: function(d) {
			b = d;
                            var d = new Date(d);
                            return true;
                        }
                    }, 'mycal');
                }
            }
            var deferred = dojo.xhrGet(xhrArgs);
        });

    </script>
  </head>
  <body class="claro">
    <div id="mycal"></div>
  </body>
</html>
