<?php
	ob_start("ob_gzhandler");

	// I should really use some sort of authentication here
	require_once("../admin/func.php");
	session_start();
	header("Content-type: application/json");
	//if (empty($_SESSION['email']))
	if (empty($_SESSION['email']) && empty($_SESSION['publickey']))
		die("Innrita fyrst");

	ini_set('display_errors',1);
	error_reporting(E_ALL);


	class Geojson 
	{
		protected $data = array();

		public function addLineString($lineString)
		{
			// Assuming the structure is like this:
			// LINESTRING(123.123 123.123)
			$lineString = substr($lineString, 11, strlen($lineString)-12);

			$points = explode(",", $lineString);

			foreach ($points as &$point)
			{
				$pointData = array_combine(array('x','y'),explode(' ',$point));
				$point = array((float)$pointData['x'], (float)$pointData['y']);
			}

//			$this->data['geometry']['type'][] = "LineString";
			$this->data['paths'][] = $points;
		}
		public function addPoint($row) 
		{
			$point = substr($row[0], 6, strlen($row[0])-1);
			$pointData = array_combine(array('x', 'y'), explode(" ", $point));
			$point = array((float)$pointData['x'], (float)$pointData['y'], $row[1]);
			$this->data['paths'][0][] = $point;
		}

		public function setSpatialReference($wkid)
		{
			$this->data['spatialReference']['wkid'] = $wkid;
		}

		public function dumpData()
		{
			return "<pre>".print_r($this->data,true)."</pre>";
		}
		public function toJson()
		{
			return json_encode($this->data);
		}
		public function clean_data() 
		{
			foreach ($this->data['paths'][0] as $i => &$point) {
				if ($i == 0) continue;

				$loc1 = $this->data['paths'][0][($i-1)];
				$loc2 = $this->data['paths'][0][($i)];
				if (isset($this->data['paths'][0][($i+1)])) {
					$loc3 = $this->data['paths'][0][($i+1)];
					if ( (sdist($loc1, $loc2) + sdist($loc2, $loc3)) > pow(sdist($loc1, $loc3), 2)) {
						unset($this->data['paths'][0][$i]);
						$this->data['paths'][0] = array_values($this->data['paths'][0]);
					}
				}

			}
		}
	}
	function sdist($loc1, $loc2) {
		$xdelta = pow(($loc1[0] - $loc2[0]),2);
		$ydelta = pow(($loc1[1] - $loc2[1]),2);
		return sqrt(($xdelta+$ydelta));
	}

	$device_id = pg_escape_string($_GET['deviceid']);
	$points = pg_escape_string($_GET['points']);
	$mail = !empty($_SESSION['email']) ? $_SESSION['email'] : $_SESSION['publickey'];
	$userID = get_user_id($mail);
	
	if (!user_can_read($userID, $device_id))
		die("Hesin brúkari hevur ikki rættindi til hesa eindina");


	$dbconn = pg_connect("host=localhost port=5432 dbname=tracking user=postgres");

	if (!$dbconn)
		echo "Ein feilur er hendur, kann ikki knýta til dátugrunn.";


       //to_char(date.date, \'DD/MM, HH24:MI\') as datetime, 
	$result = pg_query('
		SELECT st_asewkt(the_geom), to_char("TimeStamp", \'MM/DD/YYYY HH24:MI:SS\') as ts, "DeviceID"
		FROM "Tracking"
		WHERE "DeviceID" = '.intval($device_id).'
		ORDER BY "ID" DESC
		LIMIT '.intval($points).'
	');

	if (!$result)
		die("DB-ERROR!");
	
	$geojson = new geojson();

	/*if ($row = pg_fetch_row($result)) {
		$row = current($row);
		$geojson->addLineString($row);
	}*/

	while ($row = pg_fetch_row($result)) {
		//die(print_r($row,true));
		$geojson->addPoint($row);
	}

	$geojson->setSpatialReference(32629);

	//$geojson->clean_data();
	echo $geojson->toJson();
	//echo $geojson->dumpData();

