(function () {
    "use strict";
    /* JS Lint */
    // var dojo, dijit, clearTimeout, esri, window, map, resizeTimer, mtConfig, mtData;
    dojo.require("dijit.layout.BorderContainer");
    dojo.require("dijit.layout.ContentPane");
    dojo.require("esri.map");
    dojo.require("esri.symbol");
    dojo.require("dijit.form.Slider");

    var map, resizeTimer, mtData;

    mtData = {
        devices: []
    };

    function hideTime() {
        if (dojo.isIE <= 8) {
            return;
        }
        var times = dojo.doc.getElementsByClassName("timeonMap");
        dojo.forEach(times, function (time) {
            time.parentNode.removeChild(time);
        });
    }

    // Draw a route for every registered device
    function drawRoute(polylineJson, device) {
        var rgb, polylineSymbol, runningRoute, color, time, timeC, marker;
        //console.log("drawRoute: " + new Date().getMilliseconds());

        // Define the color
        rgb = device.RGB.substr(1);
        rgb = rgb.substr(0, rgb.length - 1);
        rgb = rgb.split(",");
        color = new dojo.Color([rgb[0], rgb[1], rgb[2], 0.6]);


        // Update the time
        time = dojo.byId("time" + device.id);
        time.removeChild(time.firstChild);
        timeC = dojo.doc.createTextNode(device.datetime);
        time.appendChild(timeC);

        polylineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, color, 3);
        runningRoute = new esri.geometry.Polyline(polylineJson);
        map.graphics.add(new esri.Graphic(runningRoute, polylineSymbol));

        if (mtConfig.showTimeOnPoint && (dojo.isIE <= 8) === false) {
            // var start = new Date();
            marker = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE, 20, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_NULL), new dojo.Color([0, 255, 0, 0]));
            dojo.forEach(polylineJson.paths[0], function (point) {
                var geoPoint, graphic;
                geoPoint = new esri.geometry.Point(point[0], point[1], map.spatialReference);

                graphic = new esri.Graphic(geoPoint, marker);
                graphic.time = point[2];
                map.graphics.add(graphic);
            });

            // console.log("Drawing finished: " + (new Date() - start) + "ms");

            dojo.connect(map.graphics, 'onMouseOver', function (event) {
                hideTime();
                if (event.graphic.time) {
                    var time, tid, tidE, timeOnMap, timestring;
                    time = new Date(event.graphic.time);
                    timestring = time.getDate() + "/" + (parseInt(time.getMonth(), 10) + 1) + ", " + time.toLocaleTimeString();
                    //time = new Date(time.setHours(time.getHours() - 1));
                    tid = dojo.doc.createElement("div");
                    tidE = dojo.doc.createTextNode(timestring);
                    tid.appendChild(tidE);
                    tid.style.position = "absolute";
                    tid.style.top = event.clientY + "px";
                    tid.style.left = event.clientX + "px";
                    tid.style.zIndex = 1;
                    tid.className = "timeonMap";
                    timeOnMap = dojo.byId("timeOnMap");
                    timeOnMap.appendChild(tid);
                }
            });
            dojo.connect(map.graphics, 'onMouseOut', function () { hideTime(); });
        }

        if (device.url && device.width && device.height) {
            map.infoWindow.setContent("<img src='" + device.url + "' width='50px' height='50px' />");
            map.infoWindow.show(runningRoute.getPoint(0, 0));
            map.infoWindow.resize(60, 60);
        } else {
            var point, outline, symbol, graphic;

            point = runningRoute.getPoint(0, 0);
            outline = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 1);
            symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 20, outline, color);
            graphic = new esri.Graphic(point, symbol);

            map.graphics.add(graphic);

            symbol = new esri.symbol.TextSymbol(device.devicetitle).setOffset(10,15);
            graphic = new esri.Graphic(point, symbol);

            map.graphics.add(graphic);
        }
        if (mtData.devices.length === 1) {
            map.centerAt(runningRoute.getPoint(0, 0));
        }
        //console.log('Device: ' + device.id);
        //console.log('  X: ' + runningRoute.getPoint(0, 0).x);
        //console.log('  Y: ' + runningRoute.getPoint(0, 0).y);
        //console.log(runningRoute.getPoint(0, 0)); 
        var i = false;
        dojo.forEach(mtData.devices, function(val, index) {
            if (val.id === device.id) {
                i = index;
            }
        });
        if (i !== false) {
            mtData.devices[i].lastpos = runningRoute.getPoint(0, 0);
        }
    }

    function get_position() {
        //console.log("Get position: " + new Date().getMilliseconds());
        hideTime();
        //console.log("HideTime done.");
        if ((dojo.isIE < 9) === false) {
            map.graphics.clear();
           // console.log("Graphics on map cleared. ");
        }
        dojo.forEach(mtData.devices, function (device) {
            //console.log("Get line for device " + device.id + ": " + new Date().getMilliseconds());
            if (dojo.byId("visible" + device.id).checked) {
                dojo.xhrGet({
                    url: "get_last_line3.php",
                    timeout: 3000,
                    content: {
                        deviceid: device.id,
                        points: dojo.byId("sliderValue" + device.id).value
                    },
                    handleAs: 'json',
                    // The success callback with result from server
                    load: function (result) {
                        if (result.paths instanceof Array) {
                            window.setTimeout(drawRoute(result, device), 10);
                        }
                    } // End load: json
                }); // End dojo.xhrGet
            }
        });
        console.log(mtData);
        window.setTimeout(get_position, mtConfig.refreshRate);
    } // End get_position

    function slide_box(run) {
        var arrow, adminKassi, adminKassiHeight, adminKassiWidth;
        arrow = dojo.byId("arrow");
        adminKassi = dojo.byId("admin");
        if (dojo.isIE <= 8) {
            adminKassi.style.display = "none";
        }
        adminKassiHeight = window.getComputedStyle(adminKassi).height;
        adminKassiWidth = window.getComputedStyle(adminKassi).width;
        adminKassi.style.height = adminKassiHeight;

        function showorhide() {
            var styr_trackarum = dojo.byId("styr_trackarum");
            if (parseInt(adminKassi.style.height, 10) === 10) {
                dojo.animateProperty({
                    node: "admin",
                    properties: {
                        height: {
                            end: parseInt(adminKassiHeight, 10)
                        },
                        width: {
                            end: parseInt(adminKassiWidth, 10)
                        }
                    }
                }).play();
                styr_trackarum.style.display = "";
                arrow.style.backgroundImage = "url(/img/arrow/down.png)";
            } else {
                dojo.animateProperty({
                    node: "admin",
                    properties: {
                        height: {
                            end: 10
                        },
                        width: {
                            end: 10
                        }
                    }
                }).play();
                styr_trackarum.style.display = "none";
                arrow.style.backgroundImage = "url(/img/arrow/up.png)";
            }
        }
        if (!run) {
            arrow.style.backgroundImage = "url(/img/arrow/down.png)";
            dojo.connect(arrow, 'onclick', showorhide);
        } else {
            showorhide();
        }
    }

    function centerOnTracker(device) {
        var point;
        dojo.forEach(mtData.devices, function(val, index) {
            if (val.id === device.id) {
                point = val.lastpos;
            }
        });
	map.centerAt(new esri.geometry.Point(point, map.spatialReference));
    }

    function displayDevices() {
        var container = dojo.byId("admin");

        dojo.forEach(mtData.devices, function (device) {
            var rgb, color, now, deviceInfo, br, name, nameContent, checkbox, timespan, time, slider, sliderValue;

            rgb = device.RGB.substr(1);
            rgb = rgb.substr(0, rgb.length - 1);
            rgb = rgb.split(",");

            color = new dojo.Color([rgb[0], rgb[1], rgb[2], 0.6]);
            now = new Date();

            deviceInfo = dojo.doc.createElement("div");

            br = dojo.doc.createElement("br");
            deviceInfo.appendChild(br);

            name = dojo.doc.createElement("div");
            name.style.color = color.toHex();
            name.style.cursor = 'pointer';
            nameContent = dojo.doc.createTextNode(device.devicetitle + ", ");

            timespan = dojo.doc.createElement("span");
            timespan.setAttribute("id", "time" + device.id);
            time = dojo.doc.createTextNode(device.datetime);
            timespan.appendChild(time);

            name.appendChild(nameContent);
            name.appendChild(timespan);

            deviceInfo.appendChild(name);

            checkbox = dojo.doc.createElement("input");
            checkbox.setAttribute("type", "checkbox");

            //if (device.datetime.substr(0,2) == now.getDate())
            checkbox.checked = true;

            checkbox.setAttribute("id", "visible" + device.id);
            deviceInfo.appendChild(checkbox);

            dojo.connect(name, "onclick", function () {
                checkbox.checked = true;
                centerOnTracker(device);
            });

            dojo.connect(checkbox, "onclick", function (evt) {
                if (evt.ctrlKey || evt.metaKey) { // CTRL on windows, CMD on mac
                    dojo.query('input[type=checkbox]', container).forEach(function (box) {
                        box.checked = checkbox.checked;
                    });
                }
                get_position();
            });


            slider = dojo.doc.createElement("div");
            slider.setAttribute("id", "slider" + device.id);
            deviceInfo.appendChild(slider);

            sliderValue = dojo.doc.createElement("input");
            sliderValue.setAttribute("type", "textbox");
            sliderValue.style.display = "none";
            sliderValue.setAttribute("value", "2");
            sliderValue.setAttribute("id", "sliderValue" + device.id);
            deviceInfo.appendChild(sliderValue);

            dojo.place(deviceInfo, container, "last");

            dojo.addOnLoad(function () {
                dijit.form.HorizontalSlider({
                    name: "slider" + device.id,
                    value: 2,
                    minimum: mtConfig.slider.minValue,
                    maximum: mtConfig.slider.maxValue,
                    intermediataChanges: true,
                    style: "width: 200px;",
                    onChange: function (value) {
                        dojo.byId("sliderValue" + device.id).value = value;
                        get_position();
                    }
                }, "slider" + device.id);
            });

        });
        slide_box();
    } // End function displayDevices


    function get_devices() {
        dojo.xhrGet({
            url: "get_devices.php",
            timeout: 5000,
            handleAs: 'json',
            load: function (result) {
                mtData.devices = result;
                displayDevices();
                get_position();
            }
        });
    }


    function init() {
        get_devices();
        map = new esri.Map("map", { extent: mtConfig.initialExtent, logo: false});

        dojo.forEach(mtConfig.maps, function (layer) {
            map.addLayer(layer);
        });

        dojo.connect(window, 'onresize', function () {
            clearTimeout(resizeTimer);
            resizeTimer = window.setTimeout(function () {
                map.resize();
            }, 200);
        });
    } // End function init

    dojo.ready(init);
}());

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-26546469-1']);
_gaq.push(['_setSiteSpeedSampleRate', 50]);
_gaq.push(['_trackPageview']);

(function () {
    'use strict';
    var ga, s;
    ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
}());

