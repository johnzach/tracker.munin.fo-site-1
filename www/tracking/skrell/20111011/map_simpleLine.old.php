<?php
	session_start();
	if (empty($_SESSION['email']))
		header("Location: /admin/admin.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="http://serverapi.arcgisonline.com/jsapi/arcgis/2.4/js/dojo/dijit/themes/tundra/tundra.css" />
    <style>
      html, body { height: 100%; width: 100%; margin: 0; padding: 0; }
      #map {
        margin: 0; 
        padding: 0;
	background-color: rgb(153, 179, 204);
      }
      #admin { 
	font-family: sans-serif; 
	color: white; 
	opacity: 0.8; 
	border-radius: 10px; 
	background-color: #777; 
	padding: 1.5em; 
	position: absolute;
	right: 2em; 
	top: 2em; 
	z-index: 100000;
      }
      input[type=checkbox] {
        float: right;
        /*margin-right: 15px;*/
      }
    </style>
    <script>var dojoConfig = { parseOnLoad: true }; </script>
    <script src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=2.4"></script>
    <script>
      "use strict";
      /* JS Lint */
      // var dojo, dijit, clearTimeout, setTimeout, setInterval, clearInterval, esri;
      dojo.require("dijit.layout.BorderContainer");
      dojo.require("dijit.layout.ContentPane");
      dojo.require("esri.map");
      dojo.require("esri.symbol");
      dojo.require("dijit.form.Slider");

      
      var map, resizeTimer, debug;
      debug = false;
      var deviceParams;
      function init() {
        var initExtent, basemapURL, basemap, Poll, p; 
        initExtent = new esri.geometry.Extent({"xmin": 584390, "ymin": 6868745, "xmax": 638036, "ymax": 6891755, "spatialReference": {"wkid": 32629}});
        map = new esri.Map("map", { extent: initExtent, logo: false});

//      basemapURL = "http://www.kort.fo:8089/ArcGIS/rest/services/framloga/tk_grundkort/MapServer"; // Munin grundkort
//      basemap = new esri.layers.ArcGISTiledMapServiceLayer(basemapURL);

        basemapURL = "https://www.kort.fo/ArcGIS/rest/services/Munin/MuninKortF20_2/MapServer"; // Munin grundkort
        basemap = new esri.layers.ArcGISDynamicMapServiceLayer(basemapURL);
        map.addLayer(basemap);

        dojo.connect(map, 'onLoad', function () { 
          dojo.connect(dijit.byId('map'), 'resize', function () {  
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function () {
              map.resize();
              map.reposition();
            }, 500);
          });
        });

        Poll = function (pollFunction, intervalTime) {
          var intervalId = null;

          this.start = function (newPollFunction, newIntervalTime) {
            pollFunction = newPollFunction || pollFunction;
            intervalTime = newIntervalTime || intervalTime;

            if (intervalId) {
              this.stop();
            }
            intervalId = setInterval(pollFunction, intervalTime);
          };

          this.stop = function () {
            clearInterval(intervalId);
          };
        };

        // Draw a route for every registered device
        function drawRoute(polylineJson, device) {
          var rgb, polylineSymbol, runningRoute;
          rgb = device.RGB.substr(1);
          rgb = rgb.substr(0, rgb.length - 1);
          rgb = rgb.split(",");
          var color = new dojo.Color([rgb[0], rgb[1], rgb[2], 0.6]); 
	
          polylineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, color, 3);
          runningRoute = new esri.geometry.Polyline(polylineJson);
          map.graphics.add(new esri.Graphic(runningRoute, polylineSymbol));
          map.graphics.add(new esri.Graphic(runningRoute.getPoint(0,0), new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND, 20, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,0]), 1), color)));

/*
          map.graphics.add(new esri.Graphic(
            runningRoute.getPoint(0, 0),
            new esri.symbol.TextSymbol(device.devicetitle + "\n\r\t" + device.datetime).setColor(
              new dojo.Color([rgb[0], rgb[1], rgb[2]])
            ).setFont(
              new esri.symbol.Font("14pt").setWeight(esri.symbol.Font.WEIGHT_BOLD)
            )
          )
            ); */

        }


        function get_position(devices) {
          map.graphics.clear();
          dojo.forEach(devices, function (device) {
            if (dojo.byId("visible"+device.id).checked) { 
              dojo.xhrGet({
                url: "get_last_line.php",
                // Allow only 1 second to get position
                timeout: 3000,
                content: {
                  deviceid: device.id,
                  points: dojo.byId("sliderValue"+device.id).value
                },
                // The success callback with result from server
                load: function (json) {
                  var result = dojo.fromJson(json);
	          if (result.paths instanceof Array)
	            drawRoute(result, device);
                } // End load: json
              }); // End dojo.xhrGet
            }
          });
        } // End get_position


        function get_devices() {
          dojo.xhrGet({
            url: "get_devices.php",
            timeout: 1000,
            load: function (json) {
              var devices = dojo.fromJson(json);
              displayDevices(devices);
              get_position(devices);
            }
          });
        }

        // Poll for new devices and positions every 3 seconds
        p = new Poll(get_devices, 5000);
        p.start();
      }
      function displayDevices(devices) {
	if (debug == true)
		return;
	debug = true;
        var container = dojo.byId("admin");

        dojo.forEach(devices, function(device) {
          var rgb, color, checked, now;

          rgb = device.RGB.substr(1);
          rgb = rgb.substr(0, rgb.length - 1);
          rgb = rgb.split(",");

          color = new dojo.Color([rgb[0], rgb[1], rgb[2], 0.6]); 
          checked = "";
          now = new Date();
	  if (device.datetime.substr(0,2) == now.getDate())
		checked = " checked";

          var deviceInfo = dojo.create("div", { innerHTML: "<span style='color: "+color.toHex()+";'>" + device.devicetitle + ", " + device.datetime + "</span><input type='checkbox'"+checked+" id='visible"+device.id+"' /><div id='slider" + device.id+"'> </div><input type='textbox' style='display: none;' value=2 id='sliderValue"+device.id+"' /><br />"});
          dojo.place(deviceInfo, container, "last");

          dojo.addOnLoad(function() {
            var pointSlider = new dijit.form.HorizontalSlider({
              name: "slider"+device.id,
              value: 2,
              minimum: 2,
              maximum: 1000,
              intermediataChanges: true,
              style: "width: 200px;",
              onChange: function(value) {
                dojo.byId("sliderValue"+device.id).value = value;
              }
	    }, "slider"+device.id);
          });
	
        });
      }

      dojo.ready(init);
    </script>
  </head>
  
  <body class="tundra">
    <div data-dojo-type="dijit.layout.BorderContainer" 
         data-dojo-props="design:'headline',gutters:false" 
         style="width: 100%; height: 100%; margin: 0;">
      <div id="map" 
           data-dojo-type="dijit.layout.ContentPane" 
           data-dojo-props="region:'center'"> 

        <div id="admin">
          <a href="/admin/admin.php?manage_trackers=true" style="color:white; font-decoration: none;">Stýr trackarum</a>
        </div>
      </div>
    </div>
  </body>
</html>


