<?php   
        require_once("../admin/func.php");
        $session = new Session();
        require_once("public_access.php");
        if (empty($_SESSION['email']) && empty($publickey)) // I should implement at least two modes. Embedded and full browser.
            header("Location: /admin/admin.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
    <title>GPS-Tracking</title>
    <link rel="stylesheet" href="http://serverapi.arcgisonline.com/jsapi/arcgis/2.8/js/dojo/dijit/themes/tundra/tundra.css" />
    <link rel="stylesheet" href="/tracking/style_new.css" media="all" />
    <script>
        var dojoConfig = { isDebug:true, parseOnLoad: true }; 
    </script>
    <script src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=2.8"></script>
    <script>
        // MuninTrackerConfig
        var mtConfig = {
            refreshRate: 10000,
            showTimeOnPoint: true,
            maps: [
                new esri.layers.ArcGISTiledMapServiceLayer("http://ags10.kort.fo/ArcGIS/rest/services/munin/Muninkort-vegir2/MapServer")
            ],
            initialExtent: new esri.geometry.Extent({
                "xmin": 594126.2429514859, 
                "ymin": 6890757, 
                "xmax": 597682, 
                "ymax": 6892791.137498939, 
                "spatialReference": {
                    "wkid": 32629
                }
            }),
            slider: {
                minValue: 2,
                maxValue: 1000,
                enabled: false
            },
            send_messages: false,
            slide_box: false,
            lineService: 'http://tracker.munin.fo:8001/'
        };
    </script>
    <style>
        input[type=checkbox] {
            display: none;
        }
        #log {
            display: none;
        }
    </style>
    <!--[if IE]>
    <style type="text/css">
        #admin {
            background:transparent;
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#cc646464,endColorstr=#cc646464);
            zoom: 1;
            width: 212px;
        } 
    </style>
    <![endif]-->
    <script src="trackerdev.js"></script>
  </head>

  <body class="tundra">
    <div data-dojo-type="dijit.layout.BorderContainer" data-dojo-props="design:'headline',gutters:false" style="width: 100%; height: 100%; margin: 0;">
      <div id="map" data-dojo-type="dijit.layout.ContentPane" data-dojo-props="region:'center'">
        <div id="timeOnMap"></div>
        <div id="admin">
          <?php if ($_SESSION['email']): ?>
          <a href="/admin/admin.php?manage_trackers=true" id="styr_trackarum">S</a>
          <?php endif; ?>
          <span id="arrow"></span>
        </div>
      </div>
    </div>
    <div id="copyright"><a href="http://munin.fo" target="_blank" title="Vitja munin.fo">&copy; Munin 2012</a></div>
  </body>
</html>
