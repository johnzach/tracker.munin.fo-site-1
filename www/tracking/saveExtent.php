<?php
    require_once("../admin/func.php");
    $session = new Session();

    if (isset($_REQUEST['type']) && $_REQUEST['type'] == "extent") {
        $_SESSION['xmax'] = $_REQUEST['xmax'];
        $_SESSION['xmin'] = $_REQUEST['xmin'];
        $_SESSION['ymax'] = $_REQUEST['ymax'];
        $_SESSION['ymin'] = $_REQUEST['ymin'];

    }
    else
        header('HTTP/1.0 400 Bad Request', true, 400);

