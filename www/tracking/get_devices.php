<?php
    require_once("../admin/func.php");
    $session = new Session();
    header("Content-type: application/json");

    if (empty($_SESSION['email']) && empty($_SESSION['publickey'])) {
    	die("Innrita fyrst!");
    }


	if ($_SESSION['email']) {
		$mail = $_SESSION['email'];
	}
	else {
		$mail = $_SESSION['publickey'];
	}

	$userID = get_user_id($mail);
	$devices = get_devices($userID, true);

	echo json_encode($devices);
