<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    function clean_urls(&$value, &$key) {
        $value = urldecode($value);
    }

class Kort extends CI_Controller {
    public $data;
    function __construct() {
        parent::__construct();
        $this->data = array();
    }

    public function stevna() {
        $this->data['url'] = $this->uri->uri_to_assoc(2);
        array_walk($this->data['url'], 'clean_urls');

        if ($this->data['url']['stevna'] == "Sundalagsstevna")
            $this->load->view('kort', $this->data);
    }


}

