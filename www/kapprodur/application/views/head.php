<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="http://tracker.munin.fo/assets/css/bootstrap.css" />
        <link rel="stylesheet" href="http://tracker.munin.fo/assets/css/docs.css" />
        <link rel="stylesheet" href="http://tracker.munin.fo/assets/css/prettify.css" />

        <title><?php echo (isset($title)) ? $title : "Kappróður"; ?></title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script src="/assets/js/bootstrap-rowlink.js"></script>
        <script src="/assets/js/google-code-prettify/prettify.js"></script>
    </head>
    <body>
        <div class="container">

