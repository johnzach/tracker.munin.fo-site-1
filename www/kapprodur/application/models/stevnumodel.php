<?php

class Stevnumodel extends CI_Model {
    function getStevnur() {
        $query = $this->db->get('rsf_stevna');
        return $query->result();
    }
    function addStevna($stevna) {
        if (isset($stevna['navn']) && isset($stevna['map_area'])) {
            $this->db->insert('rsf_stevna', $stevna);
            return true;
        }
        return false;
    }
    
}
