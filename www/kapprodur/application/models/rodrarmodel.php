<?php

class Rodrarmodel extends CI_Model {
    function getRodrar() {
        $this->db->select('rsf_rodur.*, rsf_stevna.navn stevna')->from('rsf_rodur')->join('rsf_stevna', 'rsf_stevna.id = rsf_rodur.stevna_id');
        $query = $this->db->get();
        return $query->result();
    }
    function addRodur($rodur) {
        if (isset($rodur['navn']) && isset($rodur['map_area'])) {
            $this->db->insert('rsf_rodur', $rodur);
            return true;
        }
        return false;
    }
    
}
