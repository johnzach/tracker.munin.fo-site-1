<?php include("head.php"); ?>

<h1>Kort uppsetan</h1>



<form>
    <table class="editLayers">
        <tr>
            <th>Tænasta</th>
            <th>Slag</th>
            <th></th>
        </tr>
        <tr class="addLayer">
            <td>
                <input type="text" name="mapService" placeholder="http://<MyServerName>/ArcGIS/rest/services/<MyServiceName>" />
            </td>
            <td>
                <select name="type">
                    <option>ArcGISTiledMapServiceLayer</option>
                    <option>ArcGISDynamicMapServiceLayer</option>
                </select>
            </td>
            <td>
                <input type="submit" value="Stovna">
            </td>
        </tr>
        <?php
            foreach($layers as $layer) {
                echo "<tr>";
                    $id = array_shift($layer);
                    foreach ($layer as $field) {
                        echo "<td>" . $field . "</td>";
                    }
                    echo "<td><input type='button' value='Strika' /></td>";
                echo "</tr>";
            }
        ?>
    </table>
</form>


<p style="padding-top: 40px;">GG. Í verandi løtu ber bert til at leggja ArcGIS Server Web Service kort inn.<p>


<?php include("footer.php"); ?>
