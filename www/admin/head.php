<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title><?= (isset($title)) ? $title : "Innrita" ?></title>

        <link rel="stylesheet" href="/css/style.css" />
        <script type="text/javascript" src="/assets/jscolor/jscolor.js"></script>
        <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-26546469-1']);
          _gaq.push(['_setSiteSpeedSampleRate', 50]);
          _gaq.push(['_trackPageview']);

          (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

    </script>
</head>
<body>
<div id="container">
<div id="main">
<?php if (!isset($noLogo)): ?>
        <img src="/img/logo.png" alt="munin.fo" id="logo" />
<?php endif; ?>
