<?php
require_once(".db_connect.php");
require_once('session.php');

function authenticate_user($args)
{
        if (is_valid($args))
        {
                $_SESSION['email'] = $args['email'];
                if (isset($_POST['remember'])) {
                    setcookie('logged_in', $args['email'], time()+60*60*24*14);
                }
        }
        else
        {
                $message = "<h3>Skeift loyniorð</h3>";
                die(require("loginform.php"));
        }
}

function is_valid($args)
{
        foreach ($args as &$arg) // Sanitize!
                $arg = pg_escape_string($arg);

        extract($args);
        $passwordhash = sha1($password); // I should use a salt

        $sql = 'SELECT "Name"
                FROM "User"
                WHERE "Email" = \''.$email.'\'
                AND "Password" = \''.$passwordhash.'\'';

        $result = pg_query($sql);
	if (!$result)
		die("DB-Error: ".pg_last_error());

        return (bool)(pg_num_rows($result) > 0);
}

function get_user_id($email)
{
        $email = pg_escape_string($email);
        $result = pg_query('SELECT "ID" FROM "User" WHERE "Email" = \''.$email.'\'');
	if (!$result)
		die ("DB-Error");
        $row = pg_fetch_object($result);
        return $row->ID;
}
function logout()
{
        setcookie('logged_in', false, time()-60*60);
        session_destroy();
        header("Location: https://" . $_SERVER['HTTP_HOST'] . "/api/logout");
}
function get_devices($userID, $onlyActive = false)
{
    $onlyActive = true;
	$devices = array();
    $sql = '
SELECT 
    "Device"."ID" as id
  , "Device"."Name" as devicetitle
  , "User"."Name" as username
  , "Symbology"."RGB"
  , "Device"."IMEI" imei
FROM
    "User"
  , "Symbology"
  , "Device" 
    ' . ($onlyActive ? "INNER" : "LEFT OUTER") . ' JOIN (
        SELECT "DeviceID"
        FROM "Tracking" ';
    if ($onlyActive) {
        $sql .= 'WHERE "TimeStamp" > NOW() - interval \'4 days\' ';
    }
    $sql .= 'GROUP BY "DeviceID"
     ) as tt ON tt."DeviceID" = "Device"."ID"
     LEFT OUTER JOIN extra_view_permissions evp ON evp.reads_from_user = "Device"."UserID"
WHERE 1=1
  AND "Device"."UserID" = "User"."ID"
  AND (
    "Device"."UserID" = '.intval($userID).' OR
      (
        evp.user_id = '.intval($userID).'
        AND "Device"."UserID" = evp.reads_from_user
      )
    )
  AND "Device"."SymbologyID" = "Symbology"."ID" 
ORDER BY "Device"."Name" ';



    $result = pg_query($sql);

	if (!$result)
		die("DB-ERROR! \n" . $sql);

	while ($row = pg_fetch_assoc($result)) {
		$row['lastpos'] = array(
			'spatialReference' => array(
				'wkid' => 32629
			),
			'type' => 'point',
			'x' => (double) (isset($row['x']) ? $row['x'] : ''), 
			'y' => (double) (isset($row['y']) ? $row['y'] : '')
		);

		unset($row['x']);
		unset($row['y']);
		$devices[] = $row;
	}

	return $devices;
}
function manage_trackers()
{
	$userID = get_user_id($_SESSION['email']);
	$devices = get_devices($userID);

	if (empty($_GET['deviceid']))
		die(include("manage_form.php"));


	// From now on, we're assuming the form has been posted 

	// The user has access edit

	$rgbColor = "{".toDec(pg_escape_string($_POST['farva']))."}";
	$deviceID = $_GET['deviceid'];
	$name = pg_escape_string($_POST['devicetitle']);


	$sql = 'UPDATE "Device" 
			SET "Name" = \''.$name.'\'
		WHERE "ID" = '.$deviceID.'
		RETURNING "SymbologyID"';

	$result = pg_query($sql);
	if (!$result)
		die("Feilur við dátugrunni".pg_last_error());

	$SymbologyID = current(pg_fetch_row($result));
	
	
	$sql = 'UPDATE "Symbology"
			SET "RGB" = \''.$rgbColor.'\'
		WHERE "ID" = '.$SymbologyID;

	$result = pg_query($sql);
	if (!$result)
		die("Feilur við dátugrunni".pg_last_error());

	if ($_FILES['image']['name']) {
		// Til fílnøvn verður eitt 'først-til-mølle' prinsipp implementera.
		$images = "../images/";
		$filename = $_FILES['image']['name'];

		while (is_file($images.$filename))
			$filename = "0".$filename;

		//if (!copy($_FILES['image']['tmp_name'], $images.$filename))
		if (!move_uploaded_file($_FILES['image']['tmp_name'], $images.$filename))
			die("Kann ikki goyma fílu");

		$url = '/images/'.$filename;

		$sql = 'SELECT * FROM "PictureMarker" WHERE device_id = '.intval($deviceID);
		$result = pg_query($sql);

		if (pg_num_rows($result) > 0)
			$sql = 'UPDATE "PictureMarker" SET url = \''.$url.'\' WHERE device_id = '.intval($deviceID);
		else
			$sql = 'INSERT INTO "PictureMarker" (url, device_id, width, height) VALUES (\''.$url.'\', '.intval($deviceID).', 50, 50)';

		$result = pg_query($sql);
		if (!$result)
			die("Kann ikki seta inn mynd");
	}


	header("Location: ".$_SERVER['HTTP_REFERER']);


	die();
}
function toHex($rgb)
{
	// Will accept RGB in this format: {200,60,60}

	$rgb = explode(',',substr($rgb, 1,strlen($rgb)-2));

	$hex = str_pad(dechex($rgb[0]), 2, "0").str_pad(dechex($rgb[1]), 2, "0").str_pad(dechex($rgb[2]), 2, "0");

	return $hex;

}
function toDec($hex)
{
	$r = hexdec(substr($hex, 0, 2));
	$g = hexdec(substr($hex, 2, 2));
	$b = hexdec(substr($hex, 4, 2));

	return $r.",".$g.",".$b;
}
function greet_user($email)
{
    $title = "Umsiting";
	require_once("welcome.php");

	die();
}
function administer_layers($email)
{

    ini_set('display_errors', 1);
    $sql = 'SELECT id, url, type
            FROM mapservices m INNER JOIN "User" u ON u."ID" = m.userid
            WHERE u."Email" = \'' . pg_escape_string($email) . '\''; 

    $result = pg_query($sql);
    if (pg_num_rows($result) == 0) {
        $layers = array(
            array('-1', "http://ags10.kort.fo/ArcGIS/rest/services/munin/Muninkort-vegir2/MapServer", "ArcGISTiledMapServiceLayer"),
            array('-2', "http://ags10.kort.fo/ArcGIS/rest/services/us_adr/MapServer", "ArcGISDynamicMapServiceLayer")
        );
    } else {
        $layers = pg_fetch_all($result);
    }

    

    $noLogo = true;
    require_once("layers.php");
    die();
}
function create_user()
{
	if (empty($_POST['Name']))
		die(include("userform.php"));

	$name = pg_escape_string($_POST['Name']);
	$email = pg_escape_string($_POST['email']);
	$password = pg_escape_string($_POST['password']);

	// Here should I check if I have permissions to create a user

	$sql = 'INSERT INTO "User" ("Name", "Email", "Password")
		VALUES (\''.$name.'\', \''.$email.'\', \''.sha1($password).'\');';

	if (!pg_query($sql))
		die("Query error");

	header("Location: ?");

	die();
}
/*
	Params:
		$_POST['Name']
		$_POST['IMEI']
		$_POST['farva']
*/
function add_unit()
{
	$userID = get_user_id($_SESSION['email']);
	if (empty($_POST['Name']))
		die(include("add_unit.php"));

	$rgb = "{".toDec(pg_escape_string($_POST['farva']))."}";

	$result = pg_query('INSERT INTO "Symbology" ("RGB") 
			    VALUES (\''.$rgb.'\')
			    RETURNING "ID"');
	if (!$result)
		die("Database query error ".pg_last_error());

	$SymbologyID = current(pg_fetch_row($result));

	$result = pg_query('INSERT INTO "Device" (
			"UserID", 
			"Name", 
			"IMEI", 
			"SymbologyID"
		)
		VALUES(
			'.intval($userID).', 
			\''.pg_escape_string($_POST['Name']).'\',
			\''.pg_escape_string($_POST['IMEI']).'\', 
			'.intval($SymbologyID).'
		);');

	if (!$result)
		die("Database query error". pg_last_error());
	

	header("Location: ?action=manage_trackers");
	die(); // This should be reduntant now. Shouldn't it?
}
function user_can_read($UserID, $DeviceID) {
	$devices = get_devices($UserID);

	foreach ($devices as $device)
	{
		if ($device['id'] == $DeviceID)
			return true;
	}
	return false;
}
