/*global module, require, console */
/** @module routes/devices */

/**
 * Device route
 * @constructor
 */
function DeviceRoute(app, client, androidSockets) {
    "use strict";

    var url  = require('url'),
        DeviceModel = require('../models/devices.js'),
        deviceModel = new DeviceModel(client, androidSockets.open);

    function serveResult(req, res, response, theme) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');

        if (req.params.format === undefined) {
            res.render('../../views/' + theme, response);
        }
        if (req.params.format === '.json') {
            res.contentType('application/json');
            var filename = 'devices';
            res.setHeader("Content-disposition", "inline; filename=" + filename + ".json");
            res.end(JSON.stringify(response));
        }
    }


    // In Runscope
    app.get('/api/devices:format(.json)?', function (req, res) {
        var response = {devices: []},
            urlquery = url.parse(req.url, true).query,
            query = client.query(
                'SELECT d."ID" AS id, d."Name" AS name ' +
                    'FROM "Device" d ' +
                    'INNER JOIN tokens ' +
                    '    ON tokens.user_id = d."UserID" ' +
                    '    WHERE tokens.token = $1',
                [urlquery.token]
            );

        query.on('row', function (device) {
            response.devices.push(device);
        });
        query.on('end', function () {
            serveResult(req, res, response, 'devices.jade');
        });
    });

    app.get('/api/device/:id([0-9]+):format?', function (req, res) {
        var response = {},
            query,
            urlquery = url.parse(req.url, true).query;

        response.connected = deviceModel.isConnected(req.params.id);
        serveResult(req, res, response, 'devices.jade');

    });

    // In Runscope
    app.get('/api/device/status/:id([0-9]+):format(.json)?', function (req, res) {
        var response = {
            connected: false,
            deviceStatus: false
        },
            gotResults = false,
            servedResults = false,
            template = 'status.jade';

        if (deviceModel.isConnected(req.params.id)) {
            response.connected = true;
            deviceModel.getStatus(req.params.id, function (err, result) {
                gotResults = true;
                if (err) {
                    response.deviceStatus = err;
                } else {
                    response.deviceStatus = result;
                }
                if (!servedResults) {
                    serveResult(req, res, response, template);
                }
            });

/***
            setTimeout(function () {
                response.deviceStatus = false;
                if (!gotResults) {
                    servedResults = true;
                    serveResult(req, res, response, template);
                }
            }, 15000);
           */

        } else {
            deviceModel.getStatusReport(req.params.id, function (err, result) {
                if (!err) {
                    try {
                        response.deviceStatus = JSON.parse(result.rows[0].message);
                        response.deviceStatusAge = result.rows[0].time;
                    } catch (e) {

                    }
                }
                serveResult(req, res, response, template);
            });
        }
    });

    app.get('/api/device/info/:deviceIdentifier([0-9a-zA-Z]+):format(.json)?', function (req, res) {
        deviceModel.getByImei(req.params.deviceIdentifier, function (device) {
            if (device) {
                device = {device: JSON.parse(JSON.stringify(device))};
                delete device.device.user.publickey;
                delete device.device.user.Password;
                serveResult(req, res, device, 'deviceInfo.jade');
            } else {
                res.end("NOT FOUND");
            }
        });
    });

    app.post('/api/device/status/:deviceIdentifier', function (req, res) {
        var response = {
            identified: false,
            request: req.body.status
        };
        deviceModel.getIdFromImei(req.params.deviceIdentifier, function (deviceId) {
            if (deviceId) {
                response.identified = true;
            }

            try {
                deviceModel.storeStatusReport(deviceId, JSON.parse(req.body.status), req.body.retransmitted, function () {
                    res.contentType('application/json');
                    res.setHeader("Content-disposition", "inline; filename=status.json");
                    res.end(JSON.stringify(response));
                });
            } catch (e) {
                console.log("unable to send in status report");
            }

        });
    });

    app.post('/api/device/stacktrace/:deviceIdentifier', function (req, res) {
        console.log('/api/device/stacktrace called!!!!!');
        var response = {
            identified: false,
            request: req.body.status
        };

        deviceModel.getIdFromImei(req.params.deviceIdentifier, function (deviceId) {
            if (deviceId) {
                response.identified = true;
            }

            var filename = req.body.filename, // fill in with field from req.body.*
                stacktrace = req.body.stacktrace;

            deviceModel.storeStacktrace(deviceId, filename, stacktrace, function () {
                res.contentType('application/json');
                res.setHeader("Content-disposition", "inline; filename=stacktrace");
                res.end(JSON.stringify({
                    'status': 'OK'
                }));
            });
        });
    });
    app.get('/api/device/restart/:deviceIdentifier', function (req, res) {
        deviceModel.getByImei(req.params.deviceIdentifier, function (device) {
            if (device) {
                androidSockets.open.forEach(function (openConnection) {
                    if (parseInt(openConnection.id, 10) === parseInt(device.id, 10)) {
                        openConnection.sendMessage({type: 'restart'});
                        req.end(JSON.stringify({'status': 'ok'}));
                    }
                });
            } else {
                req.end(JSON.stringify({'status': 'not connected'}));
            }
        });
    });
    app.get('/api/device/isLommy/:id', function (req, res) {
        var isLommy = false,
            query = client.query(
                'SELECT * ' +
                    'FROM "Device" d ' +
                    'WHERE length("IMEI") = 10 ' +
                    'AND "ID" = $1',
                [req.params.id]
            );

        query.on('row', function (device) {
            isLommy = true;
        });
        query.on('end', function () {
            res.contentType('application/json');
            res.end(JSON.stringify(isLommy));
        });
    });
}

module.exports = DeviceRoute;
