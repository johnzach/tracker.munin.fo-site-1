/*jslint nomen: true*/
/*global module, require, __dirname, setTimeout, console */
module.exports = function (app, client, emitter) {
    "use strict";
    var http = require('http'),
        async = require('async'),
        Deferred = require("promised-io/promise").Deferred,
        url  = require('url'),
        winston = require('winston'),
        logger = new (winston.Logger)({
            transports: [
                new (winston.transports.File)({ filename: 'log/tasks.log' })
            ]
        }),
        mail = require('nodemailer').mail,
        TasksModel = require('../models/tasks.js'),
        tasksModel = new TasksModel(client),
        DeviceModel = require('../models/devices.js'),
        deviceModel = new DeviceModel(client),
        TaskStatus = {
            // This HAS to have a 1 to 1 correspondence with the app!
            ACCEPT: 1,
            START: 2,
            END: 3
        },
        ArcgisLib = require('../../triggers/ArcgisLib'),
        arcgisLib = new ArcgisLib({
            servername: 'srv8.kort.fo',
            updateEmitter: emitter
        });

    arcgisLib.setToken('tkr', 'tkai');
    
    function serveResult(req, res, response, theme) {
        if (req.params.format === undefined) {
            res.render(__dirname + '/../views/tasks/' + theme, response);
        }
        if (req.params.format === '.json') {
            res.contentType('application/json');
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
            res.header('Access-Control-Allow-Headers', 'Content-Type');
            var filename = "task" + (response.task !== undefined ? response.task.id : 's');
            res.end(JSON.stringify(response));
        }
    }

    // Callback(err, userId)
    function checkToken(req, callback) {
        var urlquery = url.parse(req.url, true).query,
            token = (urlquery.token || req.body.token).replace(/ /g, "+"),
            uid = null,
            query = client.query(
                'SELECT user_id FROM tokens WHERE token = $1',
                [token]
            );

        query.on('row', function (row) {
            uid = row.user_id;
        });
        query.on('end', function () {
            var error = null;
            if (uid === null) {
                error = "INVALID_TOKEN";
            }
            callback(error, uid);
        });
    }

    app.get('/api/tasks_new:format(.json)?', function (req, res) {
        var urlquery = url.parse(req.url, true).query;
        checkToken(req, function (err, userid) {
            if (err || userid === null) {
                res.end({error: 'invalid token'});
                return;
            }

            tasksModel.getTasks(userid, function (tasks) {
                tasks = JSON.parse(JSON.stringify(tasks));

                if (urlquery.task_user_id) {
                    // I somehow loose properties when i filter the obj?
                    tasks = tasks.filter(function (task) {
                        var isComplete = (task.statuses.filter(function (taskStatus) {
                            return (taskStatus.userStatus.system_status === 'END');
                        }).length > 0);

                        if (isComplete && urlquery.type === 'notEnded') {
                            return false;
                        }

                        if (task.funku_pottur.users) {
                            return task.funku_pottur.users.filter(function (user) {
                                return parseInt(user.id, 10) === parseInt(urlquery.task_user_id, 10);
                            }).length > 0;
                        }


                        return false;
                    });
                }
                serveResult(req, res, {tasks: tasks}, 'tasks.jade');
            });
        });
    });

    /**
     * Kann ikki ordliga útfasa hesa metoduna enn
     * Skal implementera filtrering pr. device áðrenn
     *
     */
    app.get('/api/tasks:format(.json)?', function (req, res) {
        var response = {
            tasks: []
        },
            urlquery = url.parse(req.url, true).query,
            query,
            get_task_deferred = new Deferred();

        
        if (urlquery.token === undefined) {
            emitter.emit('token_error', {res: res});
            return;
        }

        /**
         * Um urlquery.device finst (tað er imei nr)
         * skal eg fyrst vita um task_user
         * er sett fyri hesa eindina
         *
         * Um tað er sett, so skal eg steðga, og senda req víðari til /api/tasks2,
         * líka sum tað verður brúkt á síðuni við áarinntøkum
         *
         * Tvs. fyrst gera vit device lookup, um device er sett
         * fyri at vita um task_user er virki fyri hesa eind
         * -->
         *  a. task_user finst, redirecta til /api/tasks.json?task_user=tuid
         *  b. tað finast ikki, vit forseta sum vant.
         *
         *
         * Áðrenn eg byrji at broyta nakað í koduni, skal eg líka hava
         * sett tests upp í runscopes
         *
         */

        if (urlquery.device) {
            deviceModel.getByImei(urlquery.device, function (device) {
                device = JSON.parse(JSON.stringify(device));
                if (device && device.task_enabled && device.task_user) {
                    /**
                     * Requesting http from a http request. 
                     * Careful, we do not want recursion 
                     */
                    http.get(
                        'http://localhost:8000/api/tasks_new.json?token=' + urlquery.token + "&task_user_id=" + device.task_user_id + '&type=' + urlquery.type,
                        function (taskRes) {
                            var body = "";
                            taskRes.on('data', function (chunk) {
                                body += chunk;
                            });
                            taskRes.on('end', function () {
                                res.end(body);
                            });
                        }
                    );
                    // res.end(JSON.stringify({info: 'serve tasks...'}));
                } else {
                    get_task_deferred.resolve(device);
                }
            });
        } else {
            get_task_deferred.resolve();
        }

        get_task_deferred.then(function () {
            query = client.query(
                'SELECT ' +
                    '    id, ' +
                    '    description ' +
                    'FROM task ' +
                    'INNER JOIN tokens ' +
                    '    ON tokens.user_id = task.user_id ' +
                    'WHERE tokens.token = $1 ' +
                    'AND task.deleted is null ',
                [urlquery.token]
            );

            query.on('row', function (row) {
                response.tasks.push(row);
            });
            query.on('end', function () {
                async.parallel([
                    function (callback) {
                        // Get location
                        if (urlquery.hasOwnProperty('fields') && urlquery.fields.indexOf('location') !== -1) {
                            async.forEach(response.tasks, function (row, cb) {
                                var q;
                                row.locations = [];
                                q = client.query('SELECT description, st_x(the_geom) x, st_y(the_geom) y ' +
                                         '   FROM task_location ' +
                                         '   WHERE task_id = $1 ',
                                    [row.id]);

                                q.on('row', function (loc) {
                                    row.locations.push(loc);
                                });

                                q.on('end', function () {
                                    cb(null);
                                });
                            },
                                function (err) {
                                    callback(err);
                                });
                        } else {
                            callback(null);
                        }
                    },
                    function (callback) {
                        // Get statueses
                        if (urlquery.hasOwnProperty('fields') && urlquery.fields.indexOf('status') !== -1) {
                            async.forEach(response.tasks, function (row, cb) {
                                var q;
                                row.statuses = [];
                                q = client.query(
                                    'SELECT description, datetime, system_status ' +
                                        'FROM task_status ts ' +
                                        'INNER JOIN task_user_status tus ON ts.task_user_status_id = tus.id ' +
                                        'WHERE ts.task_id = $1',
                                    [row.id]
                                );
                                q.on('row', function (state) {
                                    row.statuses.push(state);
                                });
                                q.on('end', function () {
                                    cb(null);
                                });

                            },
                                function (err) {
                                    callback(err);
                                });
                        } else {
                            callback(null);
                        }
                    },
                    function (callback) {
                        // Get devices
                        if (urlquery.hasOwnProperty('fields') && urlquery.fields.indexOf('devices') !== -1) {
                            async.forEach(response.tasks, function (row, cb) {

                                var q;
                                row.devices = [];
                                q = client.query(
                                    'SELECT ' +
                                        '    device_id, ' +
                                        '    accepted ' +
                                        'FROM task_devices ' +
                                        'WHERE task_id = $1',
                                    [row.id]
                                );
                                q.on('row', function (device) {
                                    row.devices.push(device);
                                });
                                q.on('end', function () {
                                    cb(null);
                                });

                            },
                                function (err) {
                                    callback(err);
                                });
                        } else {
                            callback(null);
                        }
                    }],
                    function (err) {
                        var i, isEnded, j, jl;
                        // Serve results
                        async.series([
                            function (callback) {
                                if (urlquery.hasOwnProperty('type') && urlquery.type.indexOf('notEnded') !== -1) {
                                    // Iterate through tasks, and delete finished tasks
                                    for (i = 0; i < response.tasks.length; i += 1) {
                                        isEnded = false;
                                        if (response.tasks[i].hasOwnProperty('statuses') && response.tasks[i].statuses instanceof Array) {
                                            for (j = 0, jl = response.tasks[i].statuses.length; j < jl; j += 1) {
                                                if (response.tasks[i] !== undefined && response.tasks[i].statuses[j].hasOwnProperty('system_status') &&
                                                        response.tasks[i].statuses[j].system_status === 'END') {
                                                    isEnded = true;
                                                }
                                            }
                                        }
                                        if (isEnded) {
                                            response.tasks.splice(i, 1);
                                            i -= 1;
                                        }
                                    }
                                }
                                callback();
                            },
                            function (callback) {
                                if (urlquery.hasOwnProperty('device')) {

                                    deviceModel.getIdFromImei(urlquery.device, function (deviceId) {
                                        console.log("filtering results, only return for device: " + deviceId);
                                        for (i = 0; i < response.tasks.length; i += 1) {
                                            var deviceFound = false;
                                            console.log(response.tasks[i]);
                                            for (j = 0; response.tasks[i].hasOwnProperty('devices') && j < response.tasks[i].devices.length; j += 1) {
                                                console.log(response.tasks[i].devices[j].device_id + " === " + deviceId);
                                                if (response.tasks[i].devices[j].device_id === deviceId) {
                                                    deviceFound = true;
                                                }
                                            }
                                            if (!deviceFound) {
                                                response.tasks.splice(i, 1);
                                                i -= 1;
                                            }
                                        }
                                        callback();
                                    });
                                } else {
                                    callback();
                                }
                            }],
                            function () {
                                serveResult(req, res, response, 'tasks.jade');
                            });
                    });
            });
        });
    });

    app.get('/api/tasks/statuses:format?', function (req, res) {
        var query, response = {},
            urlquery = url.parse(req.url, true).query,
            result = {
                statuses: [
                    {id: null, description: "Váttað", type: "accept"}
                ]
            };

        query = client.query(
            "SELECT id, description, system_status as type " +
                "FROM task_user_status tus " +
                '    INNER JOIN "User" u on tus.user_id = u."ID" ' +
                '        INNER JOIN tokens t on t.user_id = u."ID"' +
                '        WHERE token = $1 ' +
                '    ORDER BY tus.id ASC',
            [urlquery.token]
        );
        query.on('row', function (row) {
            result.statuses.push(row);
        });
        query.on('end', function () {
            serveResult(req, res, result, 'tasks.jade');
        });
    });

    app.get('/api/tasks/:id([0-9]+):format?', function (req, res) {
        var query, response = {},
            urlquery = url.parse(req.url, true).query;

        query = client.query(
            "SELECT task.* " +
                "FROM  task " +
                'INNER JOIN tokens ' +
                '    ON task.user_id = tokens.user_id ' +
                'WHERE tokens.token = $1 ' +
                'AND deleted is null ' +
                'AND id = $2 ',
            [urlquery.token.replace(/ /g, "+"), req.params.id]
        );

        query.on('row', function (row) { response.task = row; });

        query.on('end', function () {
            if (response.task === undefined) {
                // Throw object not found error
                emitter.emit('error', {
                    message: 'Object not found',
                    res: res
                });
                return;
            }
            async.parallel({
                statuses: function (callback) {
                    var statuses = [], task_status_query = client.query(
                        'SELECT ts.datetime, tus.description, tus.system_status ' +
                            'FROM task_status ts ' +
                            'INNER JOIN task_user_status tus ' +
                            '    ON ts.task_user_status_id = tus.id ' +
                            '    WHERE ts.task_id = $1',
                        [req.params.id]
                    );
                    task_status_query.on('row', function (row) { statuses.push(row); });
                    task_status_query.on('end', function () { callback(null, statuses); });
                },
                devices: function (callback) {
                    var devices = [], device_query = client.query(
                        'SELECT device_id id, accepted ' +
                            'FROM task_devices td ' +
                            '    WHERE td.task_id = $1',
                        [req.params.id]
                    );
                    device_query.on('row', function (row) { devices.push(row); });
                    device_query.on('end', function () {
                        if (urlquery.hasOwnProperty('fields') && urlquery.fields.indexOf('geometry') !== -1) {
                            async.forEach(devices, function (device, callback) {
                                device.geometry = [];
                                var geometry_query = client.query(
                                    'SELECT "TimeStamp" datetime, st_x(the_geom) x, st_y(the_geom) y ' +
                                        'FROM "Tracking" ' +
                                        'WHERE "DeviceID" = $1 ORDER BY "TimeStamp" DESC LIMIT 100',
                                    [device.id]
                                );
                                geometry_query.on('row', function (row) {
                                    device.geometry.push(row);
                                });
                                geometry_query.on('end', function () {
                                    callback();
                                });
                            },
                                function (err) {
                                    callback(null, devices);
                                });
                        } else {
                            callback(null, devices);
                        }
                    });

                },
                locations: function (callback) {
                    if (urlquery.hasOwnProperty('fields') && urlquery.fields.indexOf('locations') !== -1) {
                        var locations = [], location_query = client.query(
                            'SELECT description, st_x(the_geom) x, st_y(the_geom) y ' +
                                'FROM task_location ' +
                                'WHERE task_id = $1',
                            [req.params.id]
                        );
                        location_query.on('row', function (row) {
                            locations.push(row);
                        });
                        location_query.on('end', function () {
                            callback(null, locations);
                        });
                    } else {
                        callback(null);
                    }
                }
            },
                function (err, results) {
                    if (response.hasOwnProperty('task')) {
                        response.task.statuses = results.statuses;
                        response.task.devices = results.devices;
                        response.task.locations = results.locations;
                    }
                    serveResult(req, res, response, 'tasks_id.jade');

                });
        });
    });

    app.post('/api/tasks', function (req, res) {
        /** req.body may look like this:
         *
         * { description: 'sadf', devices: [ '29', '26' ], locations: [{geometry: {}, attributes: {}, displayname: 'Dalastígur 7'}] }
         * { description: 'sadf', devices: [ '29', '26' ] }
         * { description: 'sadf', devices: '26' }
         *
         * BEWARE, devices might or might not be an array
         *
         * Broytingar í samband við uppgávustýring:
         * featureclass og orig_id skal verða sent saman við upplýsingunum
         * Dømi: 
         * { description: 'umvaelast', featureclass: 'torshavnar_aarinntok_uz', orig_id: 'None', task_type: 'umvaelast', task_user_comment: 'None', task_value: 'None' }
         * { description: 'umvaelast', featureclass: 'torshavnar_aarinntok_uz', orig_id: 'None', task_type: 'umvaelast', task_user_comment: 'None', task_value: 'None', task_user: 'None',  token: 'asdf'}
         */

        var query,
            i,
            urlquery = url.parse(req.url, true).query;


        // If devices is a number or a string, convert into array
        if (typeof (req.body.devices) === 'number' || typeof (req.body.devices) === 'string') {
            req.body.devices = [req.body.devices];
        }

        // Convert deviceId's to numbers
        if (req.body.devices instanceof Array) {
            req.body.devices.map(function (n) {
                return parseInt(n, 10);
            });
        }

        checkToken(req, function (err, uid) {
            /**
             * First we need to determine funkupott for uppg, if applicable 
             *
             * if req.body.task_type && req.body.task_value are available, the task should
             * be placed in a "funkupott"
             *
             * first we check if those are set
             * if they are, we look in the database to find the funkupott
             *
             * if "closing" is true, we search for a task with a matching orig_id/arcgis identifier
             * and mark it complete, by adding a ended status
             *
             * else we create the task with the found funkupott
             *
             * this means, (atm) that the tasksModel.addTask will be extended, so it can accept a argument of funkupott
             * and that we will use a deferred, that will resolve when we reach one of the following points:
             *  - task_type && task_value are not set
             *  - we have found the funkupott
             *
             * Then, the task will be added as usual.
             *
             * Further down the road a complete rewrite of the insertion method is planned
             * so we can utlize BookshelfJS to the fullest.
             *
             */
            var funkupott_deferred = new Deferred();
            // Task uses "funkupott"
            if (req.body.task_type && req.body.task_value) {
                tasksModel.getFunkupott({task_type: req.body.task_type, task_value: req.body.task_value}, function (funkupottRouterEntry) {
                    if (funkupottRouterEntry) {
                        // if closing is truthy, mark the item complete by adding a ended status
                        // and stop further task processing (by res.end && return statement)
                        // note: this will create a memory leak, leaving promises waiting (GC after return?)
                        if (funkupottRouterEntry.get('closing') === true) {
                            logger.info("TaskFunkupottur closing IS TRUE");
                            funkupott_deferred.reject();
                            logger.info('mark task as complete', {
                                featureclass: req.body.featureclass,
                                orig_id: req.body.orig_id,
                                funkupottur: funkupottRouterEntry.get('tasks_funkupottur')
                            });
                            tasksModel.endTasksByFunkupott(
                                req.body.featureclass,
                                req.body.orig_id,
                                funkupottRouterEntry.get('tasks_funkupottur'),
                                function (tasks_marked_as_complete) {
                                    res.end(JSON.stringify({message: 'marked tasks as complete'}));
                                }
                            );

                        } else {
                            logger.info("TaskFunkupottur closing IS FALSE");

                            /**
                             * Plan 9/5
                             *
                             * Vita um uppg við sama arcgis_identifier og sama funkupott finst, sum ikki er liðug.
                             * Um so er, so skal bara ID á tí uppgávuni returnerast.
                             *
                             * Evt. skal hetta henda aðrenn funkupott_deferred verður resolvaður
                             *
                             * new Task({
                             *  funkupott_id:
                             *  arcgis_identifier:
                             * })
                             *
                             * && eingin END_status finnst ikki fyri eina uppg!
                             *
                             */
                            tasksModel.getTasks(uid, function (taskCollection) {
                                logger.info("TaskFunkupottur closing IS FALSE. Got tasks!!");
                                var tasksWithSameAttr = taskCollection
                                    .filter(function FilterOutEndedTasks(task) {
                                        var statuses = task.related("statuses").filter(function (taskStatus) {
                                            return (taskStatus.related("userStatus").get("system_status") !== 'END');
                                        });

                                        // Keep record if no "END" status is found.
                                        return statuses.length === task.related("statuses").length;
                                    })
                                    .filter(function FilterOutTasksByArcgisIdentifier(task) {
                                        return (task.related("arcgis_identifier_obj").get('orig_id') === req.body.orig_id);
                                    })
                                    .filter(function FilterOutByFunkupott(task) {
                                        return task.get('funkupottur') === funkupottRouterEntry.get('tasks_funkupottur');
                                    });

                                if (tasksWithSameAttr.length === 0) {
                                    funkupott_deferred.resolve(funkupottRouterEntry.get('tasks_funkupottur'));
                                    logger.info("Eingin uppg. finst við somu geometri & funkupott, stovna nýggja.");
                                } else {
                                    logger.info("Uppg. finst við somu geometri & funkupott, returnera ID");
                                    funkupott_deferred.reject();
                                    res.end(JSON.stringify({
                                        task_id: tasksWithSameAttr[0].get('id'),
                                        info: 'Uppgáva við somu geometry og funkupottið finst. Returnerar ID.'
                                    }));
                                }
                            });
                        }
                    } else {
                        mail({
                            from: "Error reporter <no-reply@munin.fo>",
                            to: "rj@munin.fo, jz@munin.fo",
                            subject: "Ber ikki til at velja funkupott fyri uppgávu",
                            text: "Ber ikki til at stovna uppgávu.\n\nRequest: \n\n\n" + JSON.stringify(req.body, 0, 4),
                            html: "Ber ikki til at stovna uppgávu. Request: <pre>" + JSON.stringify(req.body, 0, 4) + "</pre>"
                        });
                        emitter.emit('error', {
                            res: res,
                            message: "Kundi ikki velja funkupott. Tí er uppgávan heldur ikki innsett."
                        });
                        // funkupott_deferred.resolve(null);
                    }
                });
            } else {
                funkupott_deferred.resolve(null);
            }

            funkupott_deferred.then(function (funkupottId) {
                console.log("FunkupottID: ", funkupottId);
                console.log(req.body.task_type, req.body.task_value, req.body.task_type && req.body.task_value);
                tasksModel.addTask(req.body.description, uid, funkupottId, req.body.task_user, req.body.created, function (taskId) {
                    if (taskId === undefined) {
                        emitter.emit('error', {
                            message: 'TaskID not defined.',
                            res: res
                        });
                        return;
                    }
                    async.parallel([
                        function registerDevices(cb) {
                            tasksModel.addDevices(taskId, req.body.devices, cb);
                        },
                        function registerLocations(cb) {
                            tasksModel.addLocations(taskId, req.body.locations, cb);
                        },
                        function registerArcGISLocations(cb) {
                            tasksModel.addArcGISLocation(taskId, {
                                featureclass: req.body.featureclass,
                                orig_id: req.body.orig_id
                            }, cb);
                        }
                    ], function (err, results) {
                        /***
                         * Use a deferred here
                         * Resolve immediatly if there are no devices
                         * Resolve if task is delivered to all devices
                         * If not, resolve after n seconds
                         */
                        console.log('inserted points');
                        var responses = [];

                        emitter.emit('task_created', {
                            task_id: taskId,
                            description: req.body.description,
                            devices: req.body.devices,
                            callback: function (response) {
                                console.log('Got callback from event emitter');
                                console.log('Sending callback up to async..');
                                responses.push(response);
                            }
                        });

                        /**
                         * Check if devices are connected. 
                         * Push the task to device, and await delivery report. 
                         * Waiting atmost 10 seconds.
                         */
                        if (req.body.devices) {
                            setTimeout(function () {
                                // if responses.length === devices.length,
                                // then the task is delivered to all devices
                                // and success: true will be set
                                res.end(JSON.stringify({
                                    success: (req.body.hasOwnProperty('devices') && (req.body.devices instanceof Array) && req.body.devices.length === responses.length),
                                    task_id: taskId
                                }));
                            }, 5000);
                        } else {
                            res.end(JSON.stringify({
                                task_id: taskId
                            }));
                        }
                    });
                });
            }, function () {
                // Rejected promise.
                // do nothing.
            });
        });
    });

    app.post('/api/tasks/:id([0-9]+)/status:format(.json)', function (req, res) {
        /**
         * I expect the posted data to contain: 
         * device IMEI
         * new state (text)
         *
         * Server will determine timestamp
         */

        console.log("Update status:");
        console.log(req.body);

        req.body.taskId = req.params.id;

        if (req.body.androidId !== undefined &&
                req.body.statusId !== undefined &&
                // parseInt(req.body.statusType, 10) <= 0 &&
                req.body.statusId !== 0 &&
                parseInt(req.body.statusType, 10) !== 0) {
            console.log("Got status change request..");

            var query = client.query(
                'INSERT INTO task_status (task_id, "datetime", task_user_status_id, device_id, comment) ' +
                    'SELECT $1, NOW(), $2, d."ID" as device_id, $4' +
                    'FROM "Device" d ' +
                    'WHERE "IMEI" = $3',
                [req.body.taskId, req.body.statusId, req.body.androidId, req.body.comment]
            );
            query.on('error', function (err) {
                res.end(JSON.stringify({error: 'Could not insert', params: req.body}));
            });
            query.on('end', function () {
                tasksModel.getTask(req.body.taskId, function (err, task) {
                    console.log('Emit task_status_changed: ');
                    console.log(task);
                    emitter.emit('task_status_changed', task);

                    /**
                     * If task_user_status.system_status == END,
                     * so skal møguliga dagførast í GDB (um brúkari er TK, og uppg er í funkupotti)
                     *
                     * Vit byrja við at logga uppg obj
                     *
                     */
                    logger.info('update task status', task);
                    tasksModel.getTask_orm(req.body.taskId, function (taskModel) {
                        logger.info('update task status -> got task from ORM:', taskModel);
                        // Here I have the required attributes to update in GDB
                        // servername & service path: static
                        // upprid: taskModel.arcgis_identifier_obj.get('orig_id')
                        // now I finally need to find the attribute to change
                        // which is acessible with the funkupott_router
                        //
                        // NOT reliable to use funkupott_router - should be registered as a seperate field during task creation

                        // Extract fields from model
                        var task_type = taskModel.related('funkupottur_obj').related('routerEntries').at(0).get('task_type'),
                            upprid = taskModel.related('arcgis_identifier_obj').get('orig_id'),

                            // Rest interaction:
                            rest_featureLayer = arcgisLib.getFeatureLayer('test/torshavnar_aarinntok_test/FeatureServer/0/'),
                            inntakPromise = rest_featureLayer.getFeature({upprid: upprid});

                        inntakPromise.then(function (inntak) {
                            logger.info("Fyri upprid", upprid, 'skal attributturin', task_type, 'dagførast við value', 'nei', '. Inntak: ', inntak);

                            mail({
                                from: "Error reporter <no-reply@munin.fo>",
                                to: "rj@munin.fo",
                                subject: "Dagført lutir via REST (arcgis)",
                                text: "Dagført lutir via REST (arcgis).\n\nRequest: \n\n\n" + JSON.stringify(inntak, 0, 4),
                                html: "Dagført lutir via REST (arcgis). Sett attributt " + task_type + " til nei. Request: <pre>" + JSON.stringify(inntak, 0, 4) + "</pre>"
                            });


                            inntak.updateAttribute(task_type, 'nei');
                            inntak.save(function (err) {
                                if (err) {
                                    logger.error("Unable to save VIA rest.");
                                }
                                logger.info("Sucessfully edited feature via REST");
                                console.log("Successfully saved feature!");
                            });
                        });
                    });

                    res.end(JSON.stringify({task: task, req: req.body}, 4));
                });
            });
        }
        if (req.body.androidId !== undefined && parseInt(req.body.statusType, 10) === 0) {
            console.log('Set as accepted!');
            tasksModel.setAccepted(req.body.taskId, req.body.androidId, function (err) {
                tasksModel.getTask(req.body.taskId, function (err, task) {
                    emitter.emit('task_status_changed', task);
                    res.end(JSON.stringify(req.body, 4));
                });
            });
        }
    });

    app.del('/api/tasks/:id([0-9]+):format?', function (req, res) {
        var urlquery = url.parse(req.url, true).query;
        console.log("DELETE TASK #", req.params.id);

        checkToken(req, function (err, userid) {
            var result = {info: 'about to delete task with id: ' + req.params.id, token: urlquery.token};
            if (err) {
                result.error = err;
                res.end(JSON.stringify(result, 0, 4));
                return;
            }

            result.userid = userid;

            tasksModel.removeTask(req.params.id, function (err, result) {
                if (err) {
                    res.end(JSON.stringify(err, 0, 4));
                    return;
                }
                res.end(JSON.stringify({info: 'deleted task.'}, 0, 4));
            });

        });
    });
    app.get('/api/tasks/users:format?', function (req, res) {
        tasksModel.getUsers(function (users) {
            serveResult(req, res, users, 'tasks.jade');
        });
    });

}
