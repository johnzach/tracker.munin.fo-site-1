/*jslint es5: true */
/*global module */
/** @module routes/line */

module.exports = function (app, client) {
    "use strict";
    /**
     * Retrieves points from database.
     *
     */
    function lineRoute(req, res) {
        res.contentType('application/json');
        res.setHeader("Content-disposition", "inline; filename=line.json");
        res.setHeader("Cache-Control: no-cache, must-revalidate");
        res.setHeader("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        if (req.session === undefined || req.session.email === undefined) {
            if (req.params.points === undefined) {
                res.redirect('/api/sessionCheck/line/' + req.params.id);
            } else {
                res.redirect('/api/sessionCheck/line/' + req.params.id + '/' + req.params.points);
            }
            return;
        }

        var points = 2,
            paths = [],
            query,
            sql = [
                'SELECT ',
                't."ID" ',
                ' , st_x(t.the_geom) ',
                ' , st_y(t.the_geom) ',
                ' , to_char(t."TimeStamp", \'MM/DD/YYYY HH24:MI:SS\') as ts  ',
                ' , t."DeviceID" ',
                ' , d."UserID" ',
                ' , t.speed ',
                ' , t.accuracy ',
                ' , CASE WHEN s."TrackingID" IS NOT NULL THEN \'stop\' ',
                '       ELSE null  ',
                '   END AS stop ',
                ' FROM ',
                '   "User" u ',
                '   LEFT OUTER JOIN extra_view_permissions evp ',
                '     ON evp.user_id = u."ID", ',
                '   "Tracking" t  ',
                '   INNER JOIN "Device" d on t."DeviceID" = d."ID"  ',
                '   LEFT OUTER JOIN "Stops" s ON t."ID" = s."TrackingID" ',
                ' WHERE 1=1 ',
                '  AND ( ',
                '    d."UserID" = evp.reads_from_user OR ',
                '    d."UserID" = u."ID" ',
                '  ) ',
                '  AND u."Email" = $1 ',
                '  AND t."DeviceID" = $2 ',
                ' ORDER BY "TimeStamp" DESC ',
                ' LIMIT $3'
            ].join('\n');

        if (req.params.points) {
            points = parseInt(req.params.points, 10);
        }

        if (isNaN(points) || !points) {
            res.redirect('/line/' + req.params.id);
            return;
        }


        query = client.query(
            sql,
            [req.session.email, req.params.id, points]
        );
            

        query.on('row', function (row) {
            paths.push([row.st_x, row.st_y, row.ts, row.ID, row.speed, row.accuracy, row.stop]);
        });

        query.on('end', function () {
            var result = {
                "paths": [
                    paths
                ],
                "spatialReference":
                    {
                        "wkid": 32629
                    }
            };
            res.send(JSON.stringify(result));
        });

        query.on('error', function (err) {
            res.send(500, JSON.stringify(err));
        });
    }

    /** Define route*/
    app.get('/api/line/:id([0-9]+)/:points?', lineRoute);
}
