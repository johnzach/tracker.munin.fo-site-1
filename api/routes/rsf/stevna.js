module.exports = function (app, client) {
    "use strict";
    app.get('/rsf/:stevna/:rodur', function (req, res) {
        var query = client.query('SELECT  ' +
            '"DeviceID" device_id, ' +
            '     st_x(the_geom) x, ' +
            '     st_y(the_geom) y, ' +
            '     s."RGB" rgb, ' +
            '     rsf_rel.navn, ' +
            '     rsf_rel.kvf kvf,  ' +
            '     speed,  ' +
            '     accuracy,  ' +
            '     "TimeStamp" as timestamp ' +
            ' FROM  ' +
            '     "Tracking", ( ' +
            '         SELECT ' +
            '             d."ID" AS id, ' +
            '             rsf_batar.navn, ' +
            '             rsf_batar.kvf ' +
            '         FROM  ' +
            '             "Device" AS d  ' +
            '             INNER JOIN rsf_batar  ' +
            '                 ON rsf_batar.device_id = d."ID" ' +
            '             INNER JOIN rsf_batar_rodur_rel AS rel ' +
            '                 ON rel.batur_id = rsf_batar.id ' +
            '             INNER JOIN rsf_rodur ' +
            '                 ON rel.rodur_id = rsf_rodur.id ' +
            '             INNER JOIN rsf_stevna ' +
            '                 ON rsf_rodur.stevna_id = rsf_stevna.id ' +
            '         WHERE 1=1 ' +
            '             AND rel.rodur_id = rsf_rodur.id ' +
            '             AND rel.batur_id = rsf_batar.id ' +
            '             AND rsf_rodur.navn  = \'Fimmmannafør, dreingir\' ' +
            '             AND rsf_stevna.navn = \'Sundalagsstevna\') AS rsf_rel, ' +
            '         "Device" d  INNER JOIN "Symbology" s on d."SymbologyID" = s."ID" ' +
            ' WHERE 1=1 ' +
            '     AND rsf_rel.id = "DeviceID"  ' +
            '     AND rsf_rel.id = d."ID"  ' +
            '     AND "TimeStamp" > NOW() - interval \'50 seconds\' ' +
            ' ORDER BY "DeviceID", "TimeStamp" ASC LIMIT 100'),
            points = [];

        query.on('row', function (row) {
            points.push(row);
        });

        query.on('end', function () {
            res.send(JSON.stringify(points));
        });
    });
}
