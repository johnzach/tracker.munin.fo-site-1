module.exports = function (app, client) {
    "use strict";
    function parseCookies(_cookies) {
        var cookies = {};

        if (_cookies) {
            _cookies.split(';').forEach(function (cookie) {
                var parts = cookie.split('=');
                cookies[parts[0].trim()] = (parts[1] || '').trim();
            });
        }
        return cookies;
    }

    function parseSession(_cookie) {
        var session = {};
        _cookie.substr(0, _cookie.length - 1).split(";").forEach(function (elem) {
            var index = elem.substr(0, elem.indexOf("|")),
                data = elem.substr(elem.indexOf('"') + 1).replace(/\"/, "");

            session[index] = data;
        });
        return session;
    }

    function testParseSession() {
        var ragnar, ls, ocd, rsf, hk, assert = require('assert');


        ragnar = parseSession('email|s:19:"ragnar123@gmail.com";xmax|s:16:"723991.871391076";xmin|s:16:"582968.672678012";ymax|s:17:"6954664.211328423";ymin|s:17:"6890370.332740666"');
        assert.ok(ragnar.email === "ragnar123@gmail.com", "does not parse email ragnar123@gmail.com correctly ..");
        assert.ok(ragnar.xmax === "723991.871391076", "does not parse xmax correctly ..");
        assert.ok(ragnar.xmin === "582968.672678012", "does not parse xmin correctly ..");
        assert.ok(ragnar.ymax === "6954664.211328423", "does not parse ymax correctly ..");
        assert.ok(ragnar.ymin === "6890370.332740666", "does not parse ymin correctly ..");


        ls = parseSession('email|s:14:"vaktcent@ls.fo";xmax|s:17:"732491.6279739224";xmin|s:17:"481136.9585979174";ymax|s:17:"6941253.116797901";ymin|s:17:"6806447.638853612"');
        assert.ok(ls.email === "vaktcent@ls.fo", "does not parse LS email correctly .. ");


        ocd = parseSession('email|s:10:"ocd@ocd.fo";xmax|s:17:"663005.2910845822";xmin|s:17:"557171.7460841588";ymax|s:17:"6921425.863601727";ymin|s:16:"6854618.43832021";');
        assert.ok(ocd.email === "ocd@ocd.fo", "does not parse OCD email correctly .. ");

        ocd = parseSession('email|s:10:"ocd@ocd.fo";');
        assert.ok(ocd.email === "ocd@ocd.fo", "does not parse OCD email correctly .. ");

        rsf = parseSession('email|s:19:"rsf@post.olivant.fo";xmax|s:17:"618645.4252815176";xmin|s:17:"618468.4186775043";ymax|s:17:"6888917.635043607";ymin|s:17:"6888742.745110494"');
        assert.ok(rsf.email === "rsf@post.olivant.fo", "does not parse RSF email correctly .. ");

        hk = parseSession('email|s:8:"hk@hk.fo";xmax|s:17:"638994.3055626111";xmin|s:17:"583431.6944373889";ymax|s:17:"6895711.619464906";ymin|s:17:"6864788.380535094"');
        assert.ok(hk.email === "hk@hk.fo", "does not parse HK email correctly .. ");

    }



    function getSession(req, callback) {
        var cookies = parseCookies(req.headers.cookie),
            session = {},
            query = client.query(
                "SELECT * " +
                    "FROM sessions " +
                    "WHERE id = $1",
                [cookies.PHPSESSID]
            );

        query.on('row', function (data) {
            session = parseSession(data.data);
        });

        query.on('end', function () {
            callback(session);
        });
    }

    function getUserId(email, callback) {
        var query = client.query(
            'SELECT "User"."ID" id ' +
                'FROM "User" ' +
                'WHERE "User"."Email" = $1',
            [email]
        );
        query.on('row', function (row) {
            callback(row.id);
        });
    }

    app.get('/api/sessionCheck*', function (req, res) {
        res.contentType('application/json');
        getSession(req, function (session) {
            if (session.email) {
                if (!req.hasOwnProperty('session')) {
                    console.log('req.session === undefined');
                    throw new Error("session.js: req.session not an object!");
                } else {
                    req.session.email = session.email;
                }

                if (req.params.length > 0) {
                    res.redirect("/api" + req.params[0]);
                } else {
                    res.redirect('/');
                }
            } else {
                res.send(JSON.stringify({error: 'not signed in'}), 403);
            }
        });
    });

    app.get('/api/token', function (req, res) {
        getSession(req, function (session) {
            if (session.hasOwnProperty('email')) {
                getUserId(session.email, function (user_id) {
                    require('crypto').randomBytes(48, function (ex, buf) {
                        var token = buf.toString('base64'),
                            query = client.query(
                                'INSERT INTO tokens (token, user_id) VALUES ($1, $2)',
                                [token, user_id]
                            );
                        query.on('end', function () {
                            res.contentType('application/json');
                            res.end(JSON.stringify({token: token}));
                        });
                    });
                });
            } else {
                res.send(JSON.stringify({error: 'Not signed in'}), 401);
            }
        });

    });
    app.get('/api/token/device/:deviceid', function (req, res) {
        console.log(req.params.deviceid);
        var query = client.query(
            'SELECT "UserID" as uid FROM "Device" WHERE "IMEI" = $1',
            [req.params.deviceid]
        ),
            uid = false;
        query.on('row', function (row) {
            uid = row.uid;
        });
        query.on('end', function () {
            if (uid) {
                require('crypto').randomBytes(48, function (ex, buf) {
                    var token = buf.toString('base64'),
                        query = client.query(
                            'INSERT INTO tokens (token, user_id) VALUES ($1, $2)',
                            [token, uid]
                        );
                    query.on('end', function () {
                        res.contentType('application/json');
                        res.end(JSON.stringify({token: token}));
                    });
                });
            } else {
                res.contentType('application/json');
                res.send(JSON.stringify({error: 'Not signed in'}), 401);
            }
        });
    });
}
