/*global require, console, module */
/** @module models/devices */

var Bookshelf = require('bookshelf');

Bookshelf.PG = Bookshelf.initialize({
    client: 'pg',
    connection: {
        host: 'localhost',
        user: 'postgres',
        database: 'tracking'
    }
});

var DeviceModel = function (client, connectedClients) {
    "use strict";
    var winston = require('winston'),
        logger = new (winston.Logger)({
            transports: [
                new (winston.transports.File)({ filename: 'log/models.devices.log' })
            ]
        }),

        User = Bookshelf.PG.Model.extend({
            tableName: 'User',
            idAttribute: 'ID'
        }),

        TaskUser = Bookshelf.PG.Model.extend({
            tableName: 'task_user'
        }),

        Symbology = Bookshelf.PG.Model.extend({
            tableName: 'Symbology',
            idAttribute: 'ID'
        }),

        Device = Bookshelf.PG.Model.extend({
            tableName: 'Device',
            idAttribute: 'ID',
            user: function () {
                return this.belongsTo(User, 'UserID');
            },
            symbology: function () {
                return this.belongsTo(Symbology, 'SymbologyID');
            },
            task_user: function () {
                return this.belongsTo(TaskUser, 'task_user_id');
            }
        });

    // Will return false if the unit is not connected, 
    // else it will timestamp of the last activity
    this.isConnected = function (deviceId) {
        deviceId = parseInt(deviceId, 10);

        var i, connected = false;
        for (i = 0; i < connectedClients.length; i += 1) {
            if (deviceId === connectedClients[i].id) {
                connected = true;
            }
        }
        return connected;
    };

    this.getStatus = function (deviceId, callback) {
        console.log('get status, unit', deviceId);
        logger.info('getStatus, unit ' + deviceId);
        var i, message = {
            type: 'status'
        };

        for (i = 0; i < connectedClients.length; i += 1) {
            if (parseInt(deviceId, 10) === parseInt(connectedClients[i].id, 10)) {
                console.log('sending status request to', deviceId);
                logger.info('sending status request to ' + deviceId);
                connectedClients[i].sendMessage(message);

                connectedClients[i].subscribe('status_ack', function (data) {
                    logger.info('got status response', data);
                    callback(null, data);
                });
            }
        }

    };

    this.getByImei = function (deviceImei, callback) {
        new Device({IMEI: deviceImei})
            .fetch({
                withRelated: ['user', 'symbology', 'task_user']
            })
            .then(callback);
    };

    this.getIdFromImei = function (deviceImei, callback) {
        var deviceId = false,
            query = client.query(
                'SELECT "ID" FROM "Device" WHERE "IMEI" ILIKE $1',
                [deviceImei]
            );

        query.on('row', function (data) {
            deviceId = data.ID;
        });
        query.on('end', function () {
            console.log("Device id converted, from " + deviceImei + " to " + deviceId);
            callback(deviceId);
        });
    };

    this.storeStatusReport = function (deviceId, statusReport, retransmitted, callback) {
        /**
         * Plan 25/6:
         *
         * At goyma knattstøður úr status rapportumo
         *
         * Hvussu?
         *  - úr statusReport.GPS brúka vit fylgjandi dátur:
         *    x, y, time, speed, accuracy
         *
         */
        var query;

        if (retransmitted !== undefined) {
            retransmitted = true;
        } else {
            retransmitted = false;
        }

        query = client.query(
            "INSERT INTO status_reports (device_id, message, retransmitted) " +
                "VALUES ($1, $2, $3)",
            [parseInt(deviceId, 10), JSON.stringify(statusReport), retransmitted]
        );
        query.on('error', function (err) {
            console.log(err);
        });
        query.on('end', function () {
            if (statusReport.GPS) {
                /**
                 * TODO:
                 *
                 * Skal vita um eindin er í pausu.
                 * Um so er, so skal eg bara innseta aftur í pause talvuna...
                 *
                 *
                 */
                var savePointQuery = client.query(
                    ' INSERT INTO "Tracking" ( ' +
                        ' "DeviceID", "TimeStamp", "speed", "accuracy", "the_geom" ) VALUES ' +
                        ' ($1, $2, $3, $4, ST_Transform(ST_SetSRID(ST_MakePoint($5, $6), 4326), 32629)  ) ',
                    [
                            deviceId,
                            new Date(statusReport.GPS.time),
                            statusReport.GPS.speed,
                            statusReport.GPS.accuracy,
                            statusReport.GPS.y,
                            statusReport.GPS.x
                        ]
                );

                savePointQuery.on('end', function () {
                    logger.info("successfully saved point along with tracking report. Device: " + deviceId + ". Timestamp: " + statusReport.GPS.time);
                });
                savePointQuery.on('error', function (error) {
                    logger.error('Unable to save tracking point.', error);
                });
            }

            callback();
        });
    };
    this.getStatusReport = function (deviceId, callback) {
        var query = client.query('SELECT * FROM status_reports WHERE Device_ID = $1 ORDER BY TIME DESC LIMIT 1', [deviceId], callback);
    };

    this.storeStacktrace = function (deviceId, filename, stacktrace, callback) {
        var query = client.query('INSERT INTO stacktraces (device_id, filename, stacktrace) VALUES ($1, $2, $3)',
            [parseInt(deviceId, 10), filename, stacktrace]);

        query.on('error', function (err) {
            console.log(err);
        });

        query.on('end', callback);

    };
};
module.exports = DeviceModel;

