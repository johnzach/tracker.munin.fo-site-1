/*global module, require, console, OrgSection */ // OrgSection is not global, just cross referenced in taskUser
/** @module models/tasks */

/**
 * Initialize BookshelfJS
 **/
var Bookshelf = require('bookshelf');
Bookshelf.PG = Bookshelf.initialize({
    client: 'pg',
    connection: {
        host: 'localhost',
        user: 'postgres',
        database: 'tracking'
    }
});

// Add virtuals
Bookshelf.PG.plugin('virtuals');

/**
 * TasksModel holds ORM bindings for tasks.
 *
 *  @todo Refactor all ORM code to ORM class (classes?), that can be imported across the application.
 *
 */
var TasksModel = function (client) {
        "use strict";

        var async = require('async'),
            winston = require('winston'),
            logger = new (winston.Logger)({
                transports: [
                    new (winston.transports.File)({ filename: 'log/tasks-model.log' })
                ]
            }),

            /**
             * Declare BookshelfJS models
             */

            /**
             * TaskUser
             * @type {Bookshelf.Model}
             */
            TaskUser = Bookshelf.PG.Model.extend({
                tableName: 'task_user',
                org_section: function () {
                    return this.belongsTo(OrgSection, 'org_section');
                }
            }),

            OrgSection = Bookshelf.PG.Model.extend({
                tableName: 'org_section',
                hidden: ['parent'],
                children: function () {
                    return this.hasMany(OrgSection, 'parent');
                },
                employees: function () {
                    return this.hasMany(TaskUser, 'org_section');
                },
                leader: function () {
                    return this.belongsTo(TaskUser, 'leader');
                }
            }),

            TaskFunkuPotturUserRel = Bookshelf.PG.Model.extend({
                tableName: 'task_funku_pottur_users_rel',
                idAttribute: 'task_user_id'

            }),

            TaskFunkupotturRouter = Bookshelf.PG.Model.extend({
                tableName: 'task_funkupottur_router'
            }),

            FunkuPottur = Bookshelf.PG.Model.extend({
                tableName: 'tasks_funku_pottur',
                users: function () {
                    return this.hasMany(TaskUser, 'task_funku_pottur_id').through(TaskFunkuPotturUserRel, 'id');
                },
                org_section: function () {
                    return this.belongsTo(OrgSection);
                },
                routerEntries: function () {
                    return this.hasMany(TaskFunkupotturRouter, 'tasks_funkupottur');
                }
            }),
            User = Bookshelf.PG.Model.extend({
                tableName: 'User',
                idAttribute: 'ID'
            }),
            TaskDevice = Bookshelf.PG.Model.extend({
                tableName: 'task_devices'
            }),
            TaskLocation = Bookshelf.PG.Model.extend({
                tableName: 'task_location_xy'
            }),
            TaskUserStatus = Bookshelf.PG.Model.extend({
                tableName: 'task_user_status'
            }),
            TaskStatus = Bookshelf.PG.Model.extend({
                tableName: 'task_status',
                userStatus: function () {
                    return this.belongsTo(TaskUserStatus);
                }
            }),

            ArcgisIdentifier = Bookshelf.PG.Model.extend({
                tableName: 'arcgis_identifier'
            }),

            Task = Bookshelf.PG.Model.extend({
                tableName: 'task',
                user: function () {
                    return this.belongsTo(User, 'user_id');
                },
                devices: function () {
                    return this.hasMany(TaskDevice);
                },
                locations: function () {
                    return this.hasMany(TaskLocation);
                },
                statuses: function () {
                    return this.hasMany(TaskStatus);
                },
                funku_pottur: function () {
                    return this.belongsTo(FunkuPottur, 'funkupottur');
                },
                funkupottur_obj: function () {
                    return this.belongsTo(FunkuPottur, 'funkupottur');
                },
                arcgis_identifier_obj: function () {
                    return this.belongsTo(ArcgisIdentifier, 'arcgis_identifier');
                }
            }),



            /**
             * BookshelfJS collections
             *
             */
            Users = Bookshelf.PG.Collection.extend({
                model: TaskUser
            });



        function markTaskAsComplete(taskModel, callback) {
            // Add status
            logger.info("MarkTaskAsComplete", taskModel.get('id'));
            var taskUserStatus = new TaskUserStatus({
                    user_id: taskModel.get('user_id'),
                    system_status: 'END'
                })
                    .fetch()
                    .then(function (taskUserStatus) {
                        var ts1 = new TaskStatus({
                            task_id: taskModel.get('id'),
                            datetime: 'NOW()',
                            task_user_status_id: taskUserStatus.get('id'),
                            // comment: 'By API'
                            comment: 'Uppgáva avgreidd av ' + taskModel.get('created_by') + ' við collector'
                        });
                        ts1.save().then(callback);
                    });
        }


        this.getFunkupott = function (filter, callback) {
            var fpLookup = new TaskFunkupotturRouter(filter)
                .fetch()
                .then(callback);
        };
        this.getUsers = function (callback) {
            TaskUser
                .collection()
                .fetch({withRelated: 'org_section'})
                .then(function (users) {
                    callback(users);
                });
        };

        this.getTasks = function (uid, callback) {
            Task
                .collection()
                .query(function (qb) {
                    qb.where('deleted', 'is', 'null');
                    qb.where('user_id', '=', parseInt(uid, 10));
                })
                .fetch({withRelated: [
                    'devices',
                    'locations',
                    'statuses.userStatus',
                    'funku_pottur.users',
                    'arcgis_identifier_obj'
                ]})
                .then(callback);
        };

        this.endTasksByFunkupott = function (featureclass, orig_id, funkupott, callback) {
            ArcgisIdentifier
                .collection()
                .query(function (qb) {
                    qb.where("featureclass", '=', "torshavnar_aarinntok_uz");
                    qb.where("orig_id", "=",  orig_id);
                })
                .fetch()
                .then(function (result) {
                    var ags_ids = result.pluck('id');

                    Task
                        .collection()
                        .query(function (qb) {
                            if (ags_ids.length === 0) {
                                // This could indicate, that we are trying to end a task, which has never 
                                // existed in this system.
                                logger.error('No ArgGIS identifier for orig_id', orig_id);
                                qb.whereRaw('1=2'); // Obviously false stmt
                                // Used to ensure a empty result set (if not, it would mark ALL tasks as complete).
                            } else {
                                // Wrapping in else clause, to avoid syntax error (sql)
                                qb.whereIn('arcgis_identifier', ags_ids);
                            }
                            qb.where('funkupottur', '=', funkupott);
                        })
                        .fetch({withRelated: ['statuses.userStatus']})
                        .then(function (tasks_to_be_marked_as_complete) {
                            tasks_to_be_marked_as_complete = tasks_to_be_marked_as_complete.filter(function (task) {
                                if (task.hasOwnProperty('get') && task.get('deleted')) {
                                    console.log("dropping task, is deleted.");
                                    return false;
                                }

                                var taskParsed = JSON.parse(JSON.stringify(task)),
                                    isComplete = (taskParsed.statuses.filter(function (taskStatus) {
                                        return (taskStatus.userStatus.system_status === 'END');
                                    }).length === 1);

                                return !isComplete;

                            });

                            logger.info("Mark the following tasks as complete:", tasks_to_be_marked_as_complete);
                            async.forEach(tasks_to_be_marked_as_complete, markTaskAsComplete, callback);
                        });

                });
        };

        /**
         * Old methods below
         * To be refactored to ORM code
         */
        this.getTask_orm = function (taskId, cb) {
            new Task({id: taskId})
                .fetch({withRelated: [
                    'statuses',
                    'devices',
                    'arcgis_identifier_obj',
                    'funkupottur_obj.routerEntries'
                ]})
                .then(cb);
        };

        this.getTask = function (taskId, cb) {
            var response = {task: {}},
                query = client.query(
                    "SELECT task.* " +
                        "FROM  task " +
                        'WHERE id = $1 ',
                    [taskId]
                );

            query.on('row', function (row) { response.task = row; });

            query.on('end', function () {
                if (response.task === undefined) {
                    // Throw object not found error
                    cb({type: 'error', message: 'Object not found'});
                    return;
                }
                async.parallel({
                    statuses: function (callback) {
                        var statuses = [], task_status_query = client.query(
                            'SELECT ts.datetime, tus.description, tus.system_status ' +
                                'FROM task_status ts ' +
                                'INNER JOIN task_user_status tus ' +
                                '    ON ts.task_user_status_id = tus.id ' +
                                '    WHERE ts.task_id = $1',
                            [taskId]
                        );
                        task_status_query.on('row', function (row) { statuses.push(row); });
                        task_status_query.on('end', function () { callback(null, statuses); });
                    },
                    devices: function (callback) {
                        var devices = [], device_query = client.query(
                            'SELECT device_id id, accepted ' +
                                'FROM task_devices td ' +
                                '    WHERE td.task_id = $1',
                            [taskId]
                        );
                        device_query.on('row', function (row) { devices.push(row); });
                        device_query.on('end', function () { callback(null, devices); });
                    }
                },
                    function (err, results) {
                        if (response.hasOwnProperty('task')) {
                            response.task.statuses = results.statuses;
                            response.task.devices = results.devices;
                        }
                        cb(null, response);
                    });
            });
        };


        /**
         * Marks task as compltete
         *
         * @param {integer} taskId - Task id to accept
         * @param {integer} androidId - Device identifier
         * @param {setAcceptedCallback} cb - when the query is completed and the task is marked as complete in the database, the callback function will be executed.
         */
        this.setAccepted = function (taskId, androidId, cb) {
            var query = client.query(
                'UPDATE task_devices ' +
                    'SET accepted = NOW() ' +
                    'FROM "Device" d ' +
                    'WHERE d."ID" = device_id ' +
                    'AND task_id = $1 ' +
                    'AND "IMEI" = $2',
                [taskId, androidId]
            );

            query.on('end', function () {
                cb(null);
            });
        };

        /**
         * Creates a new task
         *
         * @param {string} description
         * @param {integer} uid - User id
         * @param {interger} funkupottId
         * @param {integer} createdBy
         * @param {Date} created
         * @param {addTaskCallback} callback
         */
        this.addTask = function (description, uid, funkupottId, createdBy, created, callback) {
            if (!created) {
                created = new Date();
            }
            var taskId, query = client.query(
                'INSERT INTO task (description, user_id, funkupottur, created_by, created) ' +
                    ' VALUES ($1, $2, $3, $4, $5) ' +
                    '        RETURNING id',
                [description, uid, funkupottId, createdBy, created]
            );
            query.on('row', function (result) {
                taskId = result.id;
            });
            query.on('end', function () {
                callback(taskId);
            });
        };

        this.addDevices = function (taskId, devices, callback) {
            if (devices === undefined) {
                callback();
                return;
            }

            async.forEach(devices, function (device_id, cb) {
                var q = client.query(
                    'INSERT INTO task_devices (task_id, device_id) VALUES($1, $2)',
                    [taskId, device_id]
                );
                q.on('end', function () {
                    cb();
                });
            }, function (err) {
                callback();
            });

        };

        this.addLocations = function (taskId, locations, callback) {
            if (locations === undefined) {
                callback();
                return;
            }

            async.forEach(locations, function (loc, cb) {
                var q = client.query(
                    'INSERT INTO task_location (task_id, the_geom, description) VALUES ' +
                        '($1, ST_SetSRID(ST_MakePoint($2, $3), 32629), $4) ',
                    [taskId, loc.geometry.x, loc.geometry.y, loc.displayname]
                );

                // Override error handler, to avoid throwing exception to main thread.
                q.on('error', function () { });

                q.on('end', function () {
                    cb();
                });
            }, function () {
                callback();
            });
        };

        this.addArcGISLocation = function (taskId, loc, callback) {
            if (!loc.featureclass || !loc.orig_id || loc.orig_id === 'None') {
                console.log("UNABLE TO ADD ARCGIS IDENTIFIER. ");
                callback(null);
                return;
            }
            var aid = new ArcgisIdentifier({
                featureclass: loc.featureclass,
                orig_id: loc.orig_id
            });
            aid.fetch()
                .then(function (model) {
                    if (model === null) {
                        aid
                            .save()
                            .then(function (model) {
                                client.query(
                                    'UPDATE task SET arcgis_identifier = $1 WHERE id = $2',
                                    [model.get('id'), taskId],
                                    function (error, result) {
                                        callback(null);
                                    }
                                );
                            });
                    } else {
                        client.query(
                            'UPDATE task SET arcgis_identifier = $1 WHERE id = $2',
                            [model.get('id'), taskId],
                            function (error, result) {
                                callback(null);
                            }
                        );
                    }
                })
                .catch(function (e) {
                    console.log(e);
                });
        };

        this.removeTask = function (taskId, callback) {
            /**
             * This will remove a task, with the associated statuses, locations and devices
             */
            var q = client.query(
                'UPDATE task SET DELETED = NOW() WHERE id = $1',
                [taskId],
                callback
            );
        };
    };

module.exports = TasksModel;
