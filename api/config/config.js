module.exports = function (app, express) {
    "use strict";
    app.configure(function () {
        var MemoryStore = express.session.MemoryStore,
            sessionStore = new MemoryStore();

        app.set('views', __dirname + '/views');
        app.set('view engine', 'jade');
        app.set("view options", { layout: false });
        app.use(express.bodyParser());
        app.use(express.methodOverride());
        app.use(express.cookieParser());
        app.use(express.session({
            store: sessionStore,
            secret: 'crazysecretstuff',
            key: 'express.sid'
        }));
        app.use(express["static"](__dirname + '/public'));
        app.use(require('stylus').middleware({ src: __dirname + '/public' }));
        app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
        app.use(app.router);
    });
};
