module.exports = function (app, client, emitter) {
    "use strict";
    var io = require('socket.io'),
        socket = io.listen(8080);

    socket.sockets.on('connection', function (socket) {
        socket.on('identify', function (data) {
            console.log(data);
        });
        emitter.on('device_connected', function (event) {
            socket.emit('device_connected', event);
        });
        socket.emit('news', { hello: 'world' });
        socket.on('my other event', function (data) {
            console.log(data);
        });

        emitter.on('task_status_changed', function (task) {
            socket.emit('news', { hello: 'TASK STATUS CHANGED!' });
            socket.emit('task_status_changed', task);
        });
    });
}
