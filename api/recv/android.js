/*global console, module, require, setTimeout*/
module.exports = function (client, androidSockets, emitter) {
    "use strict";

    var net = require('net'),
        async = require('async'),
        winston = require('winston'),
        logger = new (winston.Logger)({
            transports: [
                new (winston.transports.File)({ filename: 'log/recv.android.log' })
            ]
        }),
        server,
        recv;

    recv = {
        addStop: function (device) {
            /**
             * This will violate unique constraint if no location update is received
             * in the period between stops. This is likely to happen if a person is indoors.
             *
             * This is expected behavior.
             **/
            var query = client.query(
                'INSERT INTO "Stops" ("TrackingID") ' +
                    'SELECT "ID" FROM "Tracking" WHERE "DeviceID" = $1 ORDER BY "TimeStamp" DESC LIMIT 1',
                [device.id]
            );
            query.on('error', function (err) {
                console.log(err);
            });
        },
        identify: function (data, callback) {
            client.query(
                'SELECT "ID" FROM "Device" WHERE "IMEI" = $1',
                [data.id],
                function (err, result) {
                    if (result.rows.length > 0) {
                        callback(err, result.rows[0].ID);
                    } else {
                        callback(err, false);
                    }
                }
            );

        },
        insertPointToDb: function (device, data) {
            async.waterfall([
                function (callback) {
                    // Todo: remove table TrackingData, since it is useless
                    client.query(
                        'INSERT INTO "TrackingData" ("DataString") VALUES ($1) RETURNING "ID"',
                        [JSON.stringify(data)],
                        function (err, result) {
                            callback(err, result.rows[0].ID);
                        }
                    );
                },
                function (trackingDataId, callback) {
                    if (data.hasOwnProperty('properties')) {
                        var query,
                            // Hetta má gerast tí summar android eindir senda inn eina tíð, ið er eitt døgn í framtíðini
                            // Sí http://code.google.com/p/android/issues/detail?id=23937
                            t = new Date(data.properties.time * 1000),
                            dayInMsec = 24 * 60 * 60 * 1000,
                            hourInMsec = 60 * 60 * 1000;

                        if (t - new Date() > dayInMsec - hourInMsec && t - new Date() < dayInMsec + hourInMsec) {
                            data.properties.time = (t.valueOf() / 1000) - 60 * 60 * 24;
                            console.log("Flutt eitt døgn aftur");
                        }

                        // I also need to throw away points containing 0,0
                        if (parseInt(data.coordinates[0], 10) === 0 || parseInt(data.coordinates[1], 10) === 0) {
                            callback({type: 'error',
                                message: 'Coordinates contain 0, and are likely unvaild', data: data});
                            return;
                        }

                        query = client.query(
                            'INSERT INTO "Tracking" ( ' +
                                '"TrackingDataID", "DeviceID", "TimeStamp", ' +
                                'speed, accuracy, uptime, bearing, the_geom ' +
                                ') VALUES ' +
                                '($1, $2, to_timestamp($3), $4, $5, $6, $7, ' +
                                'ST_Transform(ST_SetSRID(ST_MakePoint($8, $9), 4326), 32629)' +
                                ')',
                            [
                                    trackingDataId,
                                    device.id,
                                    (data.properties.hasOwnProperty('time') ? data.properties.time : null),
                                    data.properties.speed,
                                    data.properties.accuracy,
                                    data.properties.uptime,
                                    data.properties.bearing,
                                    data.coordinates[1], data.coordinates[0]
                                ]
                        );
                        query.on('end', function () {
                            device.insertedPoints += 1;
                            device.lastPoint = data.properties.time;
                            callback(null);
                        });
                        query.on('error', function (error) {
                            logger.error('Query error, insert into tracking', error);
                            callback(error);
                        });
                    } else {
                        console.log('No properties in message, what!'.red);
                        logger.error('No properties in message.', data);
                    }
                }
            ], function (err, result) {
                if (err !== null) {
                    logger.error(err);
                }
            });

        },
        insertStack: function (stack, device) {
            var i;
            for (i = 0; i < stack.length; i += 1) {
                recv.insertPointToDb(stack[i], device);
            }
        },
        setSent: function (data) {
            client.query(
                'UPDATE message SET sent=true WHERE id = $1',
                [data.properties.id]
            );
        }
    };


    function Device(socket) {
        this.asidx = null;
        this.lastPoint = false;
        this.id = null;
        this.ip = socket.remoteAddress;
        this.insertedPoints = 0;
        this.pointStack = [];
        this.setId = function (id) {
            this.id = id;
            emitter.emit('device_connected', this);
            // Here should I use bookshelf to load all the attributes for connected device.
        };
        logger.info('Client connected from ' + this.ip);

        this.subscribeList = [];
        this.notify = function (type, data) {
            var newList = [];
            this.subscribeList.forEach(function (e) {
                if (e.type === type) {
                    e.callback(data);
                } else {
                    newList.push(e);
                }
            });
            this.subscribeList = newList;
        };
        this.subscribe = function (type, callback) {
            this.subscribeList.push({
                type: type,
                callback: callback
            });
        };
        this.sendMessage = function (msg) {
            try {
                socket.write(JSON.stringify(msg) + "\n");
            } catch (e) {
                console.log('Unable to send task to device');
            }

        };
    }


    server = net.createServer(function (socket) {
        socket.setEncoding();
        var device = new Device(socket);
        device.asidx = androidSockets.open.push(device) - 1;


        // I might have too many event listeners here
        emitter.on('task_created', function (event) {
            var i, task;
            console.log("devices: ");
            console.log(event.devices);
            console.log("In socket scope for " + device.id);
            if (event.hasOwnProperty('devices') && event.devices instanceof Array) {
                for (i = 0; i < event.devices.length; i += 1) {
                    if (parseInt(event.devices[0], 10) === parseInt(device.id, 10)) {
                        console.log("Found device!");
                        task = {
                            device_id: device.id,
                            type: 'task',
                            id: event.task_id
                        };

                        try {
                            console.log("Trying to send through socket");
                            console.log(task);
                            socket.write(JSON.stringify(task) + "\n");

                        } catch (e) {
                            console.log('Unable to send task to device' + device.id);
                        }
                        console.log('subscribing for task_ack');
                        device.subscribe('task_ack', function () {
                            console.log('got task_ack');
                            event.callback({
                                device_id: device.id,
                                sent: true
                            });
                        });
                    }
                }
            } else {
                console.log('I should throw an error back at the user - no selected units.. ');
            }
        });

        socket.on('data', function (bdata) {
            var data,
                chunks = bdata.trim().split("\n");
            chunks.forEach(function (chunk) {
                data = false;
                try {
                    data = JSON.parse(chunk);
                } catch (e) {
                    try {
                        socket.write(JSON.stringify({type: 'error', message: 'Unable to parse json'}) + "\n");
                        logger.error('Unable to parse JSON.', chunk);
                    } catch (er) {
                        console.log(er);
                    }
                    console.log('Invalid JSON, try stitching the chunks?');
                    console.log(chunk);
                }

                if (data && data.hasOwnProperty('type')) {
                    if (data.type.toLowerCase() === 'login' && data.hasOwnProperty('id')) {
                        recv.identify(data, function (err, result) {
                            var message = {type: 'Ack', message: 'Device recognized'};

                            if (result) {
                                device.setId(result);
                                recv.insertStack(device.pointStack, result);
                            } else {
                                message = {type: 'error', message: 'Device not recognized'};
                            }
                            try {
                                socket.write(JSON.stringify(message) + "\n");
                            } catch (e) { console.log(e); }

                        });
                    }
                    if (data.type.toLowerCase() === 'point' && device.id) {
                        recv.insertPointToDb(device, data);
                    }
                    if (data.type.toLowerCase() === 'point' && device.id === false) {
                        device.pointStack.push(data);
                        try {
                            socket.write(JSON.stringify({type: 'error', message: 'Device not identified'}) + "\n");
                        } catch (err) {
                            console.log(err);
                        }
                    }
                    if (data.type.toLowerCase() === 'ack') {
                        recv.setSent(data);
                    }
                    if (data.type.toLowerCase() === 'stop' && device.id) {
                        recv.addStop(device);
                    }
                    if (data.type.toLowerCase() === 'task_ack') {
                        console.log('Task_ack recieved!');
                        device.notify('task_ack');
                    }
                    if (data.type.toLowerCase() === 'status_ack') {
                        console.log('status_ack received!');
                        device.notify('status_ack', data);
                    }
                }
            });
        });

        socket.on('close', function () {
            logger.info('client disconnected (close)', device);
            androidSockets.open.splice(device.asidx, 1);
        });

        socket.on('error', function (err) {
            console.log('Socket error');
            console.log(err);
            logger.info('Socket error', {device: device, error: err});
            socket.destroy();
        });

        socket.on('timeout', function () {
            logger.info('client disconnected (timeout)', device);
            androidSockets.open.splice(device.asidx, 1);
        });

        setTimeout(function () {
            if (device.lastPoint === false) {
                socket.destroy();
                androidSockets.open.splice(device.asidx, 1);
            }
        }, 10000);
    });

    server.listen(30002, function () {
        console.log('Android socket server listening on port 30002');
    });

};
