/*global require, console, module, setInterval */
/*jslint vars:true */
var http = require('http'),
    jQuery = require('jquery-deferred'),
    winston = require('winston'),
    logger = new (winston.Logger)({
        transports: [
            new (winston.transports.File)({ filename: 'log/arcgislib.log' })
        ]
    }),
    querystring = require('querystring');

function setIntervalAndExecute(fn, t) {
    "use strict";
    fn();
    return (setInterval(fn, t));
}

var defaults = {
    servername: 'srv8.kort.fo',
    restPath: '/arcgis/rest/services/'
};


function ArcgisLib(options) {
    "use strict";
    var self = this;

    this.updateEmitter = options.updateEmitter;
    options.restPath = options.restPath || defaults.restPath;
    this.servername = options.servername || defaults.servername;
    this.options = options;


    this.tokenReady = new jQuery.Deferred();
    this.setToken = function (username, password) {
        var resolved = false;
        setIntervalAndExecute(function () {
            var post_data = querystring.stringify({
                username: 'tkr',
                password: 'tkai',
                client: 'requestip',
                f: 'json',
                expiration: 60 // Expiration time is in minutes. Either refresh on-the-fly or by a setInterval task
            }),

                request = http.request({
                    host: 'srv8.kort.fo',
                    port: 80,
                    path: '/arcgis/tokens/generateToken',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Content-Length': post_data.length,
                        Accept: 'text/plain'
                    }
                }, function (res) {
                    res.on('data', function (chunk) {
                        // Wrap this in try-catch!
                        try {
                            var tokenObj = JSON.parse(chunk);
                            self.token = tokenObj;

                            // Tell main process that the token has just been updated. 
                            if (self.updateEmitter) {
                                self.updateEmitter.emit('arcgis_token_update', tokenObj);
                            }


                            if (!resolved) {
                                self.tokenReady.resolve(tokenObj);
                                resolved = true;
                            }
                        } catch (e) {
                            console.error("Unable to parse json from generateToken. The service is maybe down.");
                        }
                    });
                    res.on('error', function () {
                        console.error("asdf");
                    });
                });

            request.write(post_data);
            request.end();

        }, 10 * 60 * 1000);
    };

    this.Feature = function (featureObj) {
        // Container obj for changes
        this.request = {};
        this.feature = featureObj;

        this.updateAttribute = function (attribute, value) {
            this.request[attribute] = value;
        };


        this.save = function (callback) {
            // Send this.request, using servername and token from ArcgisLib
            var prop, clonedFeature = JSON.parse(JSON.stringify(this.feature));
            delete clonedFeature.geometry;

            for (prop in this.request) {
                if (this.request.hasOwnProperty(prop)) {
                    console.log(prop, this.request[prop]);
                    clonedFeature.attributes[prop] = this.request[prop];
                }
            }

            var update_request = querystring.stringify({
                token: self.token.token,
                features: JSON.stringify([clonedFeature]),
                f: 'json',
                rollbackOnFailure: true
            });

            // Looks good! ready to send to server
            console.log(update_request);
            logger.info("About to send the following updates to server: ", update_request);

            var request = http.request({
                host: 'srv8.kort.fo',
                port: 80,
                method: 'POST',
                path: '/arcgis/rest/services/torshavnar/torshavnar_aarinntok/FeatureServer/0/updateFeatures',
            //  path: '/arcgis/rest/services/test/torshavnar_aarinntok_test/FeatureServer/0/updateFeatures',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': update_request.length,
                    Accept: 'text/plain'
                }
            }, function (res) {
                var body = "";
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', function () {
                    callback(null, JSON.parse(body));
                });
            });

            request.on('error', function (e) {
                callback(e);
            });

            request.write(update_request);
            request.end();

        };
    };

    this.FeatureLayer = function (path, token) {

        // Return a promise..
        this.getFeature = function (obj) {
            var featurePromise = new jQuery.Deferred();
            // Query the layer

            
            token.then(function (tokenObj) {
                var query = querystring.stringify({
                    token: tokenObj.token,
                    where: querystring.stringify(obj),
                    outFields: 'objectid, navn, status, syningarfolk, vidmerking, matihattur, lagt_ar, globalid, next_check_date, check_date, vidmerking_felt, upprid, grabbast, reinsast, umvaelast, synad, inntak_nummar',
                    returnGeometry: true,
                    f: 'pjson'
                });


                http.get(path + "query?" + query, function (res) {
                    res.on('data', function (result) {
                        result = JSON.parse(result);
                        console.log(result);
                        if (result.error) {
                            // Token expired
                            console.log("Got query error.");
                            // Should try to refresh the token, and try to resolve the promise with new token....
                        } else {
                            var feature = result.features[0];
                            featurePromise.resolve(new self.Feature(feature));
                        }
                    });
                    res.on('error', function (err) {
                        console.log(err);
                        if (self.updateEmitter) {
                            self.updateEmitter.emit('api_exception', {
                                
                            });
                        }
                    });
                });
            });

            return featurePromise;
        };
    };

    this.getFeatureLayer = function (path) {
        return new this.FeatureLayer('http://' + this.servername + this.options.restPath + path, this.tokenReady);
    };
}


/**
 *
 * Example usage: 
 *
var arcgisLib = new ArcgisLib({
    servername: 'srv8.kort.fo'
});

arcgisLib.setToken('tkr', 'tkai');

// Returns a featureLayer instance, with the following methods: getFeature
// getFeature retuns a feature, with the following methods: updateAttribute, save
var torshavnar_aarinntok = arcgisLib.getFeatureLayer('test/torshavnar_aarinntok_test/FeatureServer/0/');

var inntakPromise = torshavnar_aarinntok.getFeature({upprid: 3});

inntakPromise.then(function (inntak) {
    "use strict";
    // Her skal grabbast, reinsast & umvaelast setast til ja/nei
    inntak.updateAttribute('vidmerking_felt', 'Ok, ok, ok');

    inntak.save(function (err) {
        if (err) {
            throw new ArcgisLib.Error();
        }
    });
});

*/


module.exports = ArcgisLib;


/**
 * Todo
 *
 * ArcgisLib skal ruddast út, soleiðis at man kann brúka tað til ymiskt. 
 * · Tvs rudda alt, ið er specifikt til áarinntøk út
 * · Síðan rudda alt, ið er specifikt til srv8 út
 *
 * Skriva API dokumentatión til library
 *
 * Gera tests, bæði til mock http, og eisini til arcgis server
 */
